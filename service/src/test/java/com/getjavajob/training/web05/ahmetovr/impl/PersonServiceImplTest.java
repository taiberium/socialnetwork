package com.getjavajob.training.web05.ahmetovr.impl;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.dao.PersonDao;
import com.getjavajob.training.web05.ahmetovr.dao.RelationDao;
import com.getjavajob.training.web05.ahmetovr.service.Impl.PersonServiceImpl;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Created by Taiberium on 17.10.2015.
 */
public class PersonServiceImplTest {

    private PersonDao personDao;
    private RelationDao relationDao;
    private PersonService personService;

    @Before
    public void setup() throws Exception {
        personDao = mock(PersonDao.class);
        relationDao = mock(RelationDao.class);
        personService = new PersonServiceImpl(personDao, relationDao);
    }

    @Test
    public void getYears_returns_integer() {
        //prepare test
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        // set birth of leo
        LocalDate birthDate = new LocalDate(1978, 9, 27);
        leo.setBirth(birthDate.toDate());
        //present time
        LocalDate now = new LocalDate();

        //get result
        int years = personService.getYears(leo);

        //test result
        assertEquals("check age", Years.yearsBetween(birthDate, now).getYears(), years);
    }

    @Test
    public void getYears_returns_null() {
        //prepare test
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");

        //no leo birth time so return null
        Integer years = personService.getYears(leo);

        assertTrue("years is null", years == null);
    }

    @Test
    public void getYears_withoutPerson() {
        boolean exceptionThrown = false;
        try {
            //no leo birth time so return null
            Integer years = personService.getYears(null);
        } catch (IllegalArgumentException e) {
            exceptionThrown = true;
        }
        assertTrue("an exception is thrown because person can't be null", exceptionThrown);
    }

    //TODO how check saved result?
    @Test
    public void makeFriends_test() {

    }

    //TODO how check saved result?
    @Test
    public void removeRelation_test() {

    }
}
