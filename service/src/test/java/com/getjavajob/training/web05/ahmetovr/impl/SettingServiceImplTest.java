package com.getjavajob.training.web05.ahmetovr.impl;

import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.dao.SettingDao;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue;
import com.getjavajob.training.web05.ahmetovr.service.Impl.SettingServiceImpl;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import org.junit.Test;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Taiberium on 17.10.2015.
 */
public class SettingServiceImplTest {

    @Test
    public void getPersonSetting_returns_PersonSettingAggregate() {
        //prepare for testing
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        SettingDao settingDao = mock(SettingDao.class);
        SettingService settingService = new SettingServiceImpl(settingDao);
        Collection<PersonSetting> settings = asList(
                new PersonSetting(leo, SettingEnumType.PROFILE_VIEW, SettingEnumValue.ALL),
                new PersonSetting(leo, SettingEnumType.FRIENDS_VIEW, SettingEnumValue.REGGED),
                new PersonSetting(leo, SettingEnumType.AGE_VIEW, SettingEnumValue.NONE),
                new PersonSetting(leo, SettingEnumType.WALL_WRITE, SettingEnumValue.FRIENDS));

        when(settingDao.getPersonSettings(leo)).thenReturn(settings);

        //get result
        PersonSettingAggregate personSettings = settingService.getPersonSettings(leo);

        //verify result
        assertTrue("profile view setting", personSettings.getProfileView() != null);
        assertTrue("friends view setting", personSettings.getFriendsView() != null);
        assertTrue("age view setting", personSettings.getAgeView() != null);
        assertTrue("wall write setting", personSettings.getWallWrite() != null);

        assertEquals("profile view val", SettingEnumValue.ALL, personSettings.getProfileView().getVal());
        assertEquals("friends view val", SettingEnumValue.REGGED, personSettings.getFriendsView().getVal());
        assertEquals("age view val", SettingEnumValue.NONE, personSettings.getAgeView().getVal());
        assertEquals("wall write val", SettingEnumValue.FRIENDS, personSettings.getWallWrite().getVal());
    }
}
