package com.getjavajob.training.web05.ahmetovr.service;

import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Country;

import java.util.Collection;

/**
 * Created by Rustam on 23.08.2015.
 */
public interface LocationService {

    Country getCountry(int id);

    Collection<Country> getAllCountries();

    City getCity(int id);
}
