package com.getjavajob.training.web05.ahmetovr.service.Impl;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.WallRecord;
import com.getjavajob.training.web05.ahmetovr.dao.WallDao;
import com.getjavajob.training.web05.ahmetovr.service.WallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Rustam on 16.08.2015.
 */
@Service
public class WallServiceImpl implements WallService {

    private final WallDao wallDao;

    @Autowired
    public WallServiceImpl(WallDao wallDao) {
        this.wallDao = wallDao;
    }

    public WallRecord save(WallRecord wallRecord) {
        return wallDao.save(wallRecord);
    }

    @Override
    public void remove(WallRecord wallRecord) {
        wallDao.remove(wallRecord);
    }

    @Override
    public WallRecord get(int id) {
        return wallDao.get(id);
    }

    @Override
    public Collection<WallRecord> getPersonWall(Person person) {
        return wallDao.getPersonWall(person);
    }

}
