package com.getjavajob.training.web05.ahmetovr.service.Impl;

import com.getjavajob.training.web05.ahmetovr.SearchFilter;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.Relation;
import com.getjavajob.training.web05.ahmetovr.dao.PersonDao;
import com.getjavajob.training.web05.ahmetovr.dao.RelationDao;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;


/**
 * Created by Rustam on 16.08.2015.
 */
@Service
public class PersonServiceImpl implements PersonService {

    private final PersonDao personDao;
    private final RelationDao relationDao;

    @Autowired
    public PersonServiceImpl(PersonDao personDao, RelationDao relationDao) {
        this.personDao = personDao;
        this.relationDao = relationDao;
    }

    public Long getPersonFriendsCount(Person person) {
        return personDao.getPersonFriendsCount(person);
    }

    public Collection<Person> getPersonFriends(Person person) {
        return personDao.getPersonFriends(person);
    }

    public Collection<Person> getPersonByFilter(SearchFilter searchFilter) {
        return personDao.findPersons(searchFilter);
    }

    public Person getPersonByEmail(String email) {
        return personDao.getPersonByEmail(email);
    }

    public Person save(Person person) {
        return personDao.save(person);
    }

    public Collection<Person> getPersons() {
        return personDao.findPersons(new SearchFilter());
    }

    public Long getPersonsNumber() {
        return personDao.getPersonsNumber();
    }

    public boolean isFriends(Person firstPerson, Person secondPerson) {
        return personDao.isFriends(firstPerson, secondPerson);
    }

    public Integer getYears(Person person) {
        if (person == null) {
            throw new IllegalArgumentException("person can't be null!");
        }
        if (person.getBirth() == null) {
            return null;
        }
        Date birthDate = person.getBirth();
        LocalDate birthdate = LocalDate.fromDateFields(birthDate);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(birthdate, now);
        return age.getYears();
    }

    public Person getPersonById(int id) {
        return personDao.get(id);
    }

    //if we made friends, we make relation entity for both persons, so we make 2 relations
    @Override
    public void makeFriends(Person firstPerson, Person secondPerson) {
        if (firstPerson == null || secondPerson == null) {
            throw new IllegalArgumentException("can't be null");
        }
        if (isFriends(firstPerson, secondPerson)) {
            return;
        }
        relationDao.save(new Relation(firstPerson, secondPerson, true));
        relationDao.save(new Relation(secondPerson, firstPerson, true));
    }

    @Override
    public void removeRelation(Person firstPerson, Person secondPerson) {
        if (!isFriends(firstPerson, secondPerson)) {
            return;
        }
        removeRelationEntity(firstPerson, secondPerson);
        removeRelationEntity(secondPerson, firstPerson);
    }

    // equals work only with two persons, does they are friends or not doesn't matter
    private void removeRelationEntity(Person firstPerson, Person secondPerson) {
        for (Relation relation : firstPerson.getRelations()) {
            if (relation.equals(new Relation(firstPerson, secondPerson, true))) {
                Relation removeRelation = relationDao.get(relation.getId());

                relationDao.remove(removeRelation);
            }
        }
    }
}
