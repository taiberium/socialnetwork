package com.getjavajob.training.web05.ahmetovr.service;


import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.WallRecord;

import java.util.Collection;

/**
 * Created by Rustam on 16.08.2015.
 */
public interface WallService {

    WallRecord save(WallRecord wallRecord);

    void remove(WallRecord wallRecord);

    WallRecord get(int id);

    Collection<WallRecord> getPersonWall(Person person);
}
