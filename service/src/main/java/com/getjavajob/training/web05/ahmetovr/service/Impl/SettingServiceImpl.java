package com.getjavajob.training.web05.ahmetovr.service.Impl;

import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.bean.Setting;
import com.getjavajob.training.web05.ahmetovr.dao.SettingDao;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Rustam on 21.08.2015.
 */
@Service
public class SettingServiceImpl implements SettingService {

    private final SettingDao settingDao;

    @Autowired
    public SettingServiceImpl(SettingDao settingDao) {
        this.settingDao = settingDao;
    }

    @Override
    public PersonSetting save(PersonSetting setting) {
        return settingDao.save(setting);
    }

    @Override
    public void remove(PersonSetting setting) {
        settingDao.remove(setting);
    }

    @Override
    public PersonSetting get(int personId) {
        return settingDao.get(personId);
    }

    @Override
    public Setting get(SettingEnumType settingEnumType) {
        return settingDao.get(settingEnumType);
    }

    @Override
    public PersonSettingAggregate getPersonSettings(Person person) {
        Collection<PersonSetting> settingCollection = settingDao.getPersonSettings(person);
        PersonSettingAggregate personSettings = new PersonSettingAggregate();
        for (PersonSetting personSetting : settingCollection) {
            SettingEnumType settingEnumType = personSetting.getSetting().getType();
            switch (settingEnumType) {
                case PROFILE_VIEW:
                    personSettings.setProfileView(personSetting);
                    break;
                case FRIENDS_VIEW:
                    personSettings.setFriendsView(personSetting);
                    break;
                case AGE_VIEW:
                    personSettings.setAgeView(personSetting);
                    break;
                case WALL_WRITE:
                    personSettings.setWallWrite(personSetting);
                    break;
            }
        }
        return personSettings;
    }

}
