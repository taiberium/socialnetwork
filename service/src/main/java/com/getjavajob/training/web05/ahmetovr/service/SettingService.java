package com.getjavajob.training.web05.ahmetovr.service;

import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.bean.Setting;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;

/**
 * Created by Rustam on 21.08.2015.
 */
public interface SettingService {

    PersonSetting save(PersonSetting setting);

    void remove(PersonSetting setting);

    PersonSetting get(int personId);

    PersonSettingAggregate getPersonSettings(Person person);

    Setting get(SettingEnumType settingEnumType);
}
