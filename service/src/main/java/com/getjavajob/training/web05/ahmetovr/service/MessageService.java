package com.getjavajob.training.web05.ahmetovr.service;

import com.getjavajob.training.web05.ahmetovr.bean.Message;
import com.getjavajob.training.web05.ahmetovr.bean.Person;

import java.util.Collection;

/**
 * Created by Rustam on 20.08.2015.
 */
public interface MessageService {

    Message save(Message message);

    void remove(Message message);

    Message get(int id);

    Collection<Message> getMessagesByPersons(Person firstPerson, Person secondPeson);
}

