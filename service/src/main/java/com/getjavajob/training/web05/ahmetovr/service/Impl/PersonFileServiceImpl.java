package com.getjavajob.training.web05.ahmetovr.service.Impl;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonFile;
import com.getjavajob.training.web05.ahmetovr.dao.PersonFileDao;
import com.getjavajob.training.web05.ahmetovr.service.PersonFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Taiberium on 16.09.2015.
 */
@Service
public class PersonFileServiceImpl implements PersonFileService {

    private final PersonFileDao personFileDao;

    @Autowired
    public PersonFileServiceImpl(PersonFileDao personFileDao) {
        this.personFileDao = personFileDao;
    }

    public void save(PersonFile personFile) {
        personFileDao.save(personFile);
    }

    public PersonFile get(int id) {
        return personFileDao.get(id);
    }

    public void remove(PersonFile personFile) {
        personFileDao.remove(personFile);
    }

    public PersonFile getByOwner(Person person) {
        return personFileDao.getByOwner(person);
    }
}
