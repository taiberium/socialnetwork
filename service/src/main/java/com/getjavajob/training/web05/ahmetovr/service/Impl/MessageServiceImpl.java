package com.getjavajob.training.web05.ahmetovr.service.Impl;

import com.getjavajob.training.web05.ahmetovr.bean.Message;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.dao.MessageDao;
import com.getjavajob.training.web05.ahmetovr.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Rustam on 20.08.2015.
 */
@Service
public class MessageServiceImpl implements MessageService {

    private final MessageDao messageDao;

    @Autowired
    public MessageServiceImpl(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Override
    public Message save(Message message) {
        return messageDao.save(message);
    }

    @Override
    public Message get(int id) {
        return messageDao.get(id);
    }

    @Override
    public void remove(Message message) {
        messageDao.remove(message);
    }

    @Override
    public Collection<Message> getMessagesByPersons(Person firstPerson, Person secondPeson) {
        return messageDao.getMessagesByPersons(firstPerson, secondPeson);
    }
}
