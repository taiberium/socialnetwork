package com.getjavajob.training.web05.ahmetovr.service;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonFile;

/**
 * Created by Taiberium on 16.09.2015.
 */
public interface PersonFileService {

    void save(PersonFile personFile);

    void remove(PersonFile personFile);

    PersonFile get(int id);

    PersonFile getByOwner(Person person);
}
