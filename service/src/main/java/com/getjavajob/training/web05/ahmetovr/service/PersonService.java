package com.getjavajob.training.web05.ahmetovr.service;


import com.getjavajob.training.web05.ahmetovr.SearchFilter;
import com.getjavajob.training.web05.ahmetovr.bean.Person;

import java.util.Collection;

/**
 * Created by Rustam on 16.08.2015.
 */
public interface PersonService {

    Long getPersonFriendsCount(Person person);

    Collection<Person> getPersonFriends(Person person);

    Person getPersonByEmail(String email);

    Person save(Person person);

    Collection<Person> getPersonByFilter(SearchFilter searchFilter);

    Collection<Person> getPersons();

    Long getPersonsNumber();

    boolean isFriends(Person firstPerson, Person secondPerson);

    Integer getYears(Person person);

    Person getPersonById(int id);

    void makeFriends(Person firstPerson, Person secondPerson);

    void removeRelation(Person firstPerson, Person secondPerson);

}
