package com.getjavajob.training.web05.ahmetovr.service.Impl;

import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.dao.CityDao;
import com.getjavajob.training.web05.ahmetovr.dao.CountryDao;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Rustam on 23.08.2015.
 */
@Service
public class LocationServiceImpl implements LocationService {

    private final CountryDao countryDao;
    private final CityDao cityDao;

    @Autowired
    public LocationServiceImpl(CountryDao countryDao, CityDao cityDao) {
        this.cityDao = cityDao;
        this.countryDao = countryDao;
    }

    @Override
    public Country getCountry(int id) {
        return countryDao.get(id);
    }
    @Override
    public City getCity(int id) {
        return cityDao.get(id);
    }

    @Override
    public Collection<Country> getAllCountries() {
        return countryDao.getCountries();
    }
}
