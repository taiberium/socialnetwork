package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.bean.Setting;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 01.10.2015.
 */
@ContextConfiguration(locations = "classpath:dao-test-context.xml")
public class SettingDaoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    SettingDao settingDao;
    @Autowired
    PersonDao personDao;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void createDBPerson() {
        //create first person
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (100, 'Petr' ,'Ivanovich', 'volf@gmail.com', '12345' ) ");
    }

    @Test
    public void get_returns_setting() throws Exception {
        // create setting with id 100 and type PROFILE_VIEW
        createSetting();

        // call get in settingDao
        final Setting setting = settingDao.get(SettingEnumType.PROFILE_VIEW);

        // check personSetting
        assertNotNull(setting);
        assertEquals("setting id", setting.getId(), 100);
        assertEquals("setting with type", setting.getType(), SettingEnumType.PROFILE_VIEW);
    }

    @Test
    public void get_returns_personSetting() throws Exception {
        // create personSetting with id 100
        createPersonSetting();

        // call get in personSettingDao
        final PersonSetting personSetting = settingDao.get(100);

        // check personSetting
        assertNotNull(personSetting);
        assertEquals("personSetting id", personSetting.getId(), 100);
        assertEquals("personSetting val", personSetting.getVal(), SettingEnumValue.FRIENDS);
    }

    @Test
    public void get_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is personSetting with id 100
        final int numberOfPersonSettings = countRowsInTableWhere("person_setting", "id = 100");
        assertEquals("there are personSetting with id 100", 0, numberOfPersonSettings);

        // get personSetting with id 100
        final PersonSetting personSetting = settingDao.get(100);

        // then null is returned
        assertNull(personSetting);
    }

    @Test
    public void remove_the_personSetting() throws Exception {
        // create personSetting with id 100
        createPersonSetting();

        // get personSetting with id 100
        final PersonSetting personSetting = settingDao.get(100);

        // and call remove()
        settingDao.remove(personSetting);
        entityManager.flush();

        // then this personSetting gets removed from the personSetting table
        final int numberOfPersonSettings = countRowsInTableWhere("person_setting", "id = 100");
        assertEquals("row with id 100 is in table", 0, numberOfPersonSettings);
    }

    @Test
    public void save_new_personSetting() throws Exception {
        //get test persons
        Person petr = personDao.get(100);
        Setting setting = settingDao.get(SettingEnumType.PROFILE_VIEW);
        // create new personSetting
        PersonSetting personSetting = new PersonSetting(petr, setting, SettingEnumValue.FRIENDS);

        // save personSetting and get savedPersonSetting instead with id not 0
        final PersonSetting savedPersonSetting = settingDao.save(personSetting);

        // a new personSetting with id gets returned
        assertTrue("id is not 0", savedPersonSetting.getId() != 0);
        entityManager.flush();

        // and a row is inserted into personSetting table
        final int numberOfPersonSettings = countRowsInTableWhere("person_setting", "id = " + savedPersonSetting.getId());
        assertEquals(1, numberOfPersonSettings);
    }

    @Test
    public void getPersonSettings_returns_personSettingCollection() throws Exception {
        //create new personSetting record
        createPersonSetting();

        //get test person and person setting
        PersonSetting personSetting = settingDao.get(100);
        Person petr = personDao.get(100);

        Collection<PersonSetting> personSettings = settingDao.getPersonSettings(petr);
        assertTrue("person personSetting contain record", personSettings.contains(personSetting));
    }

    private void createSetting() {
        //create setting
        jdbcTemplate.update("INSERT INTO " +
                "setting (id, name)" +
                " VALUES (100, 'PROFILE_VIEW') ");
    }

    private void createPersonSetting() {
        createSetting();
        //create person setting with PERSON_VIEW setting val FRIENDS
        jdbcTemplate.update("INSERT INTO person_setting (id,person_id,setting_id,val)" +
                " VALUES (100, 100,100,'FRIENDS')");
    }

}
