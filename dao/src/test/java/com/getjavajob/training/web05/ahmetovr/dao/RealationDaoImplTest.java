package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.Relation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 02.10.2015.
 */
@ContextConfiguration(locations = "classpath:dao-test-context.xml")
public class RealationDaoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    RelationDao relationDao;
    @Autowired
    PersonDao personDao;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void create2DBPersons() {
        //create first person
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (100, 'Petr' ,'Ivanovich', 'volf@gmail.com', '12345' ) ");

        //create second person
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (120, 'John' ,'Malkovich', 'malko@gmail.com', '275' ) ");
    }


    @Test
    public void get_returns_relation() throws Exception {
        // create relation with id 100
        createRelation();

        // call get in relationDao
        final Relation relation = relationDao.get(100);

        // check relation
        assertNotNull(relation);
        assertEquals("relation id", relation.getId(), 100);
    }

    @Test
    public void get_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is relation with id 100
        final int numberOfRelations = countRowsInTableWhere("relationship", "id = 100");
        assertEquals("there are relation with id 100", 0, numberOfRelations);

        // get relation with id 100
        final Relation relation = relationDao.get(100);

        // then null is returned
        assertNull(relation);
    }

    @Test
    public void remove_the_relation() throws Exception {
        // create relation with id 100
        createRelation();

        // get relation with id 100
        final Relation relation = relationDao.get(100);

        // and call remove()
        relationDao.remove(relation);
        entityManager.flush();

        // then this relation gets removed from the relation table
        final int numberOfRelations = countRowsInTableWhere("relationship", "id = 100");
        assertEquals("row with id 100 is in table", 0, numberOfRelations);
    }

    @Test
    public void save_new_relation() throws Exception {
        //get test persons
        Person petr = personDao.get(100);
        Person john = personDao.get(120);
        // create new relation
        Relation relation = new Relation(petr, john, true);

        // save relation and get savedRelation instead with id not 0
        final Relation savedRelation = relationDao.save(relation);

        // a new relation with id gets returned
        assertTrue("id is not 0", savedRelation.getId() != 0);
        entityManager.flush();

        // and a row is inserted into relation table
        final int numberOfRelations = countRowsInTableWhere("relationship", "id = " + savedRelation.getId());
        assertEquals(1, numberOfRelations);
    }

    private void createRelation() {
        jdbcTemplate.update("INSERT INTO relationship (id, person_id,opponent_id,friend) VALUES (100, 100,120,TRUE)");
    }
}
