package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Country;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 01.10.2015.
 */
@ContextConfiguration(locations = "classpath:dao-test-context.xml")
public class CountryDaoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    CountryDao countryDao;

    @Test
    public void getCountries_returns_all_countries() throws Exception {
        // create country with id 100
        jdbcTemplate.update("INSERT INTO country (id, name) VALUES (100, 'Russia')");
        // create country with id 100
        jdbcTemplate.update("INSERT INTO country (id, name) VALUES (120, 'USA')");
        // given there are some countries
        final int numberOfCountries = countRowsInTable("country");
        assertTrue("there are no countries", numberOfCountries == 2);

        // when getCountries() is called
        final Collection<Country> countries = countryDao.getCountries();

        // then number of countries returned matches with number of rows in a table
        assertEquals("number of expected countries is not equal to number of fetched countries", numberOfCountries, countries.size());
    }

    @Test
    public void get_returns_country() throws Exception {
        // create country with id 100
        jdbcTemplate.update("INSERT INTO country (id, name) VALUES (100, 'Russia')");

        // call get in countryDao
        final Country russia = countryDao.get(100);

        // check country
        assertNotNull(russia);
        assertEquals("country name", russia.getName(), "Russia");
        assertEquals("country id", russia.getId(), 100);
    }

    @Test
    public void get_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is country with id 100
        final int numberOfCountries = countRowsInTableWhere("country", "id = 100");
        assertEquals("there are country with id 100", 0, numberOfCountries);

        // get country with id 100
        final Country country = countryDao.get(100);

        // then null is returned
        assertNull(country);
    }

}
