package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 01.10.2015.
 */
@ContextConfiguration(locations = "classpath:dao-test-context.xml")
public class PersonDaoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    PersonDao personDao;

    @PersistenceContext
    EntityManager entityManager;

    @Test
    public void get_returns_person() throws Exception {
        // create person with id 100
        createDBPerson();

        // call get in personDao
        final Person petr = personDao.get(100);
        testPerson(petr);
    }

    @Test
    public void get_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is person with id 100
        final int numberOfPersons = countRowsInTableWhere("person", "id = 100");
        assertEquals("there are person with id 100", 0, numberOfPersons);

        // get person with id 100
        final Person person = personDao.get(100);

        // then null is returned
        assertNull(person);
    }

    @Test
    public void remove_the_person() throws Exception {
        // create person with id 100
        createDBPerson();

        // get person with id 100
        final Person petr = personDao.get(100);

        // and call remove()
        personDao.remove(petr);
        entityManager.flush();

        // then this person gets removed from the person table
        final int numberOfPersons = countRowsInTableWhere("person", "id = 100");
        assertEquals("row with id 100 is in table", 0, numberOfPersons);
    }

    @Test
    public void save_new_person() throws Exception {
        // create new person
        Person person = new Person("volf@gmail.com", "12345", "Petr", "Ivanovich");

        // save person and get savedPerson instead with id not 0
        final Person savedPerson = personDao.save(person);

        // a new person with id gets returned
        assertTrue("id is not 0", savedPerson.getId() != 0);
        entityManager.flush();

        // and a row is inserted into person table
        final int numberOfPersons = countRowsInTableWhere("person", "id = " + savedPerson.getId());
        assertEquals(1, numberOfPersons);
    }

    @Test
    public void getPersonByEmail_returns_person() throws Exception {
        // create person with id 100
        createDBPerson();

        // call get in personDao
        final Person petr = personDao.getPersonByEmail("volf@gmail.com");

        // check person
        testPerson(petr);
    }

    @Test
    public void getPersonByEmail_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is person with id 100
        final int numberOfPersons = countRowsInTableWhere("person", "id = 100");
        assertEquals("there are person with id 100", 0, numberOfPersons);

        // get person with id 100
        final Person person = personDao.getPersonByEmail("volf@gmail.com");

        // then null is returned
        assertNull(person);
    }

    @Test
    public void getPersonNumber_returns_long() throws Exception {
        //count persons
        final int numberOfPersonsBefore = countRowsInTable("person");
        assertEquals("person count before", 0, numberOfPersonsBefore);

        //create one new person
        createDBPerson();

        // check if count larger on one
        final int numberOfPersonsAfter = countRowsInTable("person");
        assertEquals("person count after", 1, numberOfPersonsAfter);
    }

    @Test
    public void isFriends_returns_boolean() throws Exception {
        //count persons
        final int numberOfPersonsBefore = countRowsInTable("person");
        assertEquals("person count before", 0, numberOfPersonsBefore);

        //create 2 persons with id 100 and 120
        create2DBPersons();
        //get persons
        Person petr = personDao.get(100);
        Person john = personDao.get(120);

        //check what persons are not friends yet
        assertFalse(personDao.isFriends(petr, john));
        assertFalse(personDao.isFriends(john, petr));
        //make friends persons with id 100 and 120
        makeFriends();
        //check is friends
        assertTrue("person is friends", personDao.isFriends(petr, john));
        assertTrue("person is friends", personDao.isFriends(john, petr));
    }

    @Test
    public void getPersonFriends_returns_PersonCollection() throws Exception {
        //count persons
        final int numberOfPersonsBefore = countRowsInTable("person");
        assertEquals("person count before", 0, numberOfPersonsBefore);

        //create 2 persons with id 100 and 120
        create2DBPersons();
        makeFriends();
        //get persons
        Person petr = personDao.get(100);
        Person john = personDao.get(120);

        //get friends collection
        Collection<Person> personCollection = personDao.getPersonFriends(petr);
        // check size of collection and friend in collection
        assertEquals("person collection size", 1, personCollection.size());
        assertTrue("person collection contain friend", personCollection.contains(john));
    }

    @Test
    public void getPersonFriendsCount_returns_int() throws Exception {
        //count persons
        final int numberOfPersonsBefore = countRowsInTable("person");
        assertEquals("person count before", 0, numberOfPersonsBefore);

        //create 2 persons with id 100 and 120
        create2DBPersons();
        //get person
        Person petr = personDao.get(100);

        //check what there are no friends yet
        assertEquals("check friends count before", new Long(0), personDao.getPersonFriendsCount(petr));
        //make friends persons with id 100 and 120
        makeFriends();
        //check friends count
        assertEquals("check friends count after", new Long(1), personDao.getPersonFriendsCount(petr));
    }

    //TODO searchFilter
   /* Collection<Person> findPersons(SearchFilter filter);*/

    private void createDBPerson() {
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (100, 'Petr' ,'Ivanovich', 'volf@gmail.com', '12345' ) ");
    }

    private void create2DBPersons() {
        //create first person
        createDBPerson();
        //create second person
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (120, 'John' ,'Malkovich', 'malko@gmail.com', '275' ) ");
    }


    private void makeFriends() {
        //make second person friend to first person
        jdbcTemplate.update("INSERT INTO " +
                "relationship (id, person_id,opponent_id,friend)" +
                " VALUES (50, 100,120, TRUE ) ");

        //make first person friend to second person
        jdbcTemplate.update("INSERT INTO " +
                "relationship (id, person_id,opponent_id,friend)" +
                " VALUES (150, 120 ,100, TRUE ) ");
    }


    private void testPerson(Person petr) {
        // check person
        assertNotNull(petr);
        assertEquals("person first name", petr.getFirstName(), "Petr");
        assertEquals("person last name", petr.getLastName(), "Ivanovich");
        assertEquals("person email", petr.getEmail(), "volf@gmail.com");
        assertEquals("person password", petr.getPassword(), "12345");
        assertEquals("person id", petr.getId(), 100);
    }
}
