package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonFile;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 01.10.2015.
 */
@ContextConfiguration(locations = "classpath:dao-test-context.xml")
public class PersonFileDaoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    PersonFileDao personFileDao;
    @Autowired
    PersonDao personDao;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void createDBPerson() {
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (100, 'Petr' ,'Ivanovich', 'volf@gmail.com', '12345' ) ");
    }

    @Test
    public void get_returns_personFile() throws Exception {
        // create personFile with id 100
        jdbcTemplate.update("INSERT INTO person_file (id,owner_id, name,data)" +
                " VALUES (100,100, 'testFile','12345' ) ");

        // call get in personFileDao
        final PersonFile personFile = personFileDao.get(100);

        // check personFile
        assertNotNull(personFile);
        assertEquals("personFile name", personFile.getName(), "testFile");
        assertEquals("personFile id", personFile.getId(), 100);
    }

    @Test
    public void get_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is personFile with id 100
        final int numberOfFiles = countRowsInTableWhere("person_file", "id = 100");
        assertEquals("there are personFile with id 100", 0, numberOfFiles);

        // get personFile with id 100
        final PersonFile personFile = personFileDao.get(100);

        // then null is returned
        assertNull(personFile);
    }

    @Test
    public void remove_the_personFile() throws Exception {
        // create personFile with id 100
        jdbcTemplate.update("INSERT INTO person_file (id,owner_id, name,data)" +
                " VALUES (100,100, 'testFile','12345') ");

        // get personFile with id 100
        final PersonFile personFile = personFileDao.get(100);
        // and call remove()
        personFileDao.remove(personFile);
        entityManager.flush();

        // then this personFile gets removed from the personFile table
        final int numberOfFiles = countRowsInTableWhere("person_file", "id = 100");
        assertEquals("row with id 100 is in table", 0, numberOfFiles);
    }

    @Test
    public void save_new_personFile() throws Exception {
        byte[] bytes = new byte[]{1, 2, 3};
        Person owner = personDao.get(100);
        // create new personFile
        PersonFile personFile = new PersonFile(owner, "testFile", bytes);

        // save personFile and get savedPersonFile with id not 0
        final PersonFile savedPersonFile = personFileDao.save(personFile);

        // a new personFile with id gets returned
        assertTrue("id is not 0", savedPersonFile.getId() != 0);
        entityManager.flush();

        // and a row is inserted into personFile table
        final int numberOfFiles = countRowsInTableWhere("person_file", "id = " + savedPersonFile.getId());
        assertEquals(1, numberOfFiles);
    }

    @Test
    public void getByOwner_returns_personFile() throws Exception {
        //check there is no files
        final int numberOfFiles = countRowsInTable("person_file");
        assertEquals("no person file in table", 0, numberOfFiles);

        //create new file
        save_new_personFile();
        Person owner = personDao.get(100);

        // get file by owner
        PersonFile personFile = personFileDao.getByOwner(owner);
        assertNotNull(personFile);
    }
}
