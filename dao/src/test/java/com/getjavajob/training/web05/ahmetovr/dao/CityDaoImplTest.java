package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.City;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 01.10.2015.
 */
@ContextConfiguration(locations = "classpath:dao-test-context.xml")
public class CityDaoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    CityDao cityDao;

    @Test
    public void get_returns_city() throws Exception {
        // create city with id 100
        jdbcTemplate.update("INSERT INTO city (id, name) VALUES (100, 'Ufa')");

        // call get in cityDao
        final City ufa = cityDao.get(100);

        // check city
        assertNotNull(ufa);
        assertEquals("city name", ufa.getName(), "Ufa");
        assertEquals("city id", ufa.getId(), 100);
    }

    @Test
    public void get_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is city with id 100
        final int numberOfCities = countRowsInTableWhere("city", "id = 100");
        assertEquals("there are city with id 100", 0, numberOfCities);

        // get city with id 100
        final City city = cityDao.get(100);

        // then null is returned
        assertNull(city);
    }

}
