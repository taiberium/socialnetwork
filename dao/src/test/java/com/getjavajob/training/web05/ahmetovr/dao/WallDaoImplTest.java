package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.WallRecord;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 01.10.2015.
 */
@ContextConfiguration(locations = "classpath:dao-test-context.xml")
public class WallDaoImplTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    WallDao wallDao;
    @Autowired
    PersonDao personDao;

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void create2DBPersons() {
        //create first person
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (100, 'Petr' ,'Ivanovich', 'volf@gmail.com', '12345' ) ");

        //create second person
        jdbcTemplate.update("INSERT INTO " +
                "person (id, first_name,last_name,email,password)" +
                " VALUES (120, 'John' ,'Malkovich', 'malko@gmail.com', '275' ) ");
    }

    @Test
    public void get_returns_wallRecord() throws Exception {
        // create wallRecord with id 100
        createWallRecord();

        // call get in wallRecordDao
        final WallRecord wallRecord = wallDao.get(100);

        // check wallRecord
        assertNotNull(wallRecord);
        assertEquals("wallRecord id", wallRecord.getId(), 100);
        assertEquals("wall body", wallRecord.getBody(), "Hello NIGGA!");
    }

    @Test
    public void get_returns_null_if_there_is_no_such_id() throws Exception {
        // check if there is wallRecord with id 100
        final int numberOfWallRecords = countRowsInTableWhere("wall", "id = 100");
        assertEquals("there are wallRecord with id 100", 0, numberOfWallRecords);

        // get wallRecord with id 100
        final WallRecord wallRecord = wallDao.get(100);

        // then null is returned
        assertNull(wallRecord);
    }

    @Test
    public void remove_the_wallRecord() throws Exception {
        // create wallRecord with id 100
        createWallRecord();

        // get wallRecord with id 100
        final WallRecord wallRecord = wallDao.get(100);

        // and call remove()
        wallDao.remove(wallRecord);
        entityManager.flush();

        // then this wallRecord gets removed from the wall table
        final int numberOfWallRecords = countRowsInTableWhere("wall", "id = 100");
        assertEquals("row with id 100 is in table", 0, numberOfWallRecords);
    }

    @Test
    public void save_new_wallRecord() throws Exception {
        //get test persons
        Person petr = personDao.get(100);
        Person john = personDao.get(120);
        // create new wallRecord
        WallRecord wallRecord = new WallRecord("Hello NIGGA!", petr, john);

        // save wallRecord and get savedWallRecord instead with id not 0
        final WallRecord savedWallRecord = wallDao.save(wallRecord);

        // a new wallRecord with id gets returned
        assertTrue("id is not 0", savedWallRecord.getId() != 0);
        entityManager.flush();

        // and a row is inserted into wallRecord table
        final int numberOfWallRecords = countRowsInTableWhere("wall", "id = " + savedWallRecord.getId());
        assertEquals(1, numberOfWallRecords);
    }

    @Test
    public void getPersonWall_returns_wallRecordCollection() throws Exception {
        //create new wall record
        createWallRecord();

        //get test person and wall record
        WallRecord wallRecord = wallDao.get(100);
        Person petr = personDao.get(100);

        Collection<WallRecord> wallRecords = wallDao.getPersonWall(petr);
        assertTrue("person wall contain record", wallRecords.contains(wallRecord));
    }

    private void createWallRecord() {
        jdbcTemplate.update("INSERT INTO wall (id,body,time,author_person_id,person_id)" +
                " VALUES (100, 'Hello NIGGA!','2015-05-13 07:15:31.123456789',120,100)");
    }
}
