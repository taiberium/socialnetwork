create table country (
id serial not null PRIMARY KEY,
name text not null
);

create table city (
id serial not null PRIMARY KEY,
name text not null,
country_id int references country(id)
);

create table person (
id serial not null PRIMARY KEY,
first_name text,
last_name text,
gender text,
email text not null,
birth date,
password text not null,
location int REFERENCES city(id)
);

ALTER TABLE person
  ADD CONSTRAINT unique_email UNIQUE(email);

CREATE TABLE setting (
id serial NOT NULL PRIMARY KEY,
name text  not null
);

create table message(
id serial not null PRIMARY KEY,
body text,
time timestamp not null,
author_person_id int not null references person(id),
person_id int not null references person(id),
read boolean not null
);

create table wall(
id serial not null PRIMARY KEY,
body text,
time timestamp not null,
author_person_id int not null references person(id),
person_id int not null references person(id)
);

create table comment(
id serial not null PRIMARY KEY,
body text not null,
person_id int not null references person(id)
);

create table hobby(
id serial not null PRIMARY KEY,
name text not null
);

create table person_setting (
id serial not null PRIMARY KEY,
person_id int not null references person(id),
setting_id int not null references setting(id),
val text
);

create table wall_favor(
id serial not null PRIMARY KEY,
wall_id int not null references wall(id),
person_id int not null references person(id)
);

create table comment_favor(
id serial not null PRIMARY KEY,
comment_id int not null references comment(id),
person_id int not null references person(id)
);

create table relationship (
id serial not null PRIMARY KEY,
person_id int not null references person(id),
opponent_id int not null references person(id),
friend boolean not null
);

create table person_hobby(
id serial not null PRIMARY KEY,
person_id int not null references person(id),
hobby_id int not null references hobby(id)
);

CREATE TABLE person_file(
id serial not null PRIMARY KEY,
owner_id int not null REFERENCES person(id),
name text not null,
data bytea not null
);