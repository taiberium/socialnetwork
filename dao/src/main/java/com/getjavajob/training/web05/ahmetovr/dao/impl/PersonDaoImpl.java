package com.getjavajob.training.web05.ahmetovr.dao.impl;

import com.getjavajob.training.web05.ahmetovr.SearchFilter;
import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.dao.PersonDao;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * Created by Rustam on 24.07.2015.
 */
@Transactional
@Repository
public class PersonDaoImpl implements PersonDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Person save(Person person) {
        return entityManager.merge(person);
    }

    @Override
    public void remove(Person person) {
        entityManager.remove(person);
    }

    @Override
    public Person get(int id) {
        return entityManager.find(Person.class, id);
    }

    public boolean isFriends(Person firstPerson, Person secondPerson) {
        if (firstPerson == null || secondPerson == null) {
            return false;
        }
        Collection<Person> personCollection = getPersonFriends(firstPerson);
        for (Person person : personCollection) {
            if (person.getId() == secondPerson.getId()) {
                return true;
            }
        }
        return false;
    }

    public Collection<Person> findPersons(SearchFilter filter) {
        if (filter == null) {
            throw new IllegalArgumentException("filter can't be null");
        }
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
        final Root<Person> msg = criteriaQuery.from(Person.class);
        List<Predicate> predicates = new ArrayList<>();
        if (filter.getName() != null && filter.getName().length() > 0) {
            Predicate firstNameLike = criteriaBuilder.or(
                    criteriaBuilder.like(criteriaBuilder.lower(msg.<String>get("firstName")), filter.getName().toLowerCase() + "%"),
                    criteriaBuilder.like(criteriaBuilder.lower(msg.<String>get("lastName")), filter.getName().toLowerCase() + "%")
            );
            predicates.add(firstNameLike);
        }
        if (filter.getLastName() != null && filter.getLastName().length() > 0) {
            Predicate lastNameLike = criteriaBuilder.like(criteriaBuilder.lower(msg.<String>get("lastName")), filter.getLastName().toLowerCase() + "%");
            predicates.add(lastNameLike);
        }
        if (filter.getCountry() != null) {
            if (filter.getCity() != null) {
                Predicate cityEqual = criteriaBuilder.equal(msg.<String>get("location"), filter.getCity());
                predicates.add(cityEqual);
            } else {
                Predicate countryEqual = criteriaBuilder.equal(msg.<String>get("location").<String>get("country"), filter.getCountry());
                predicates.add(countryEqual);
            }
        }
        if (filter.getSex() != null && filter.getSex().length() > 0) {
            Predicate sexEqual = criteriaBuilder.equal(criteriaBuilder.lower(msg.<String>get("gender")), filter.getSex().toLowerCase());
            predicates.add(sexEqual);
        }
        if (filter.getCity() != null) {
            Predicate locationEqual = criteriaBuilder.equal(msg.<City>get("location"), filter.getCity());
            predicates.add(locationEqual);
        }
        if (filter.getPerson() != null) {
            Predicate personFriends = msg.get("id").in(getFriendId(filter.getPerson()));
            predicates.add(personFriends);
        }
        //We search here people with age between firstAge and secondAge
        int firstAge = filter.getAge();
        int secondAge = filter.getAgeDifference();
        if (firstAge > 0 && secondAge > 0) {
            LocalDate ageDate = new LocalDate();
            LocalDate localDateFirstAge;
            LocalDate localDateSecondAge;
            if (firstAge > secondAge) {
                localDateFirstAge = ageDate.minusYears(firstAge);
                localDateSecondAge = ageDate.minusYears(secondAge);
            } else {
                localDateFirstAge = ageDate.minusYears(secondAge);
                localDateSecondAge = ageDate.minusYears(firstAge);
            }
            Date ageFrom = localDateFirstAge.toDateTimeAtStartOfDay().toDate();
            Date ageTo = localDateSecondAge.toDateTimeAtStartOfDay().toDate();
            Predicate birth = criteriaBuilder.between(msg.<java.sql.Date>get("birth"), new java.sql.Date(ageFrom.getTime()),
                    new java.sql.Date(ageTo.getTime()));
            predicates.add(birth);
        }
        criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    private Collection<Integer> getFriendId(Person person) {
        final Query query = entityManager.createQuery(
                " select r.target.id from Relation r where r.owner = :person and r.friend = true ");
        query.setParameter("person", person);
        return query.getResultList();
    }

    public Person getPersonByEmail(String email) {
        final TypedQuery<Person> query = entityManager.createQuery(
                "select p from Person p where p.email = :email", Person.class);
        query.setParameter("email", email);
        List<Person> personList = query.getResultList();
        return personList.isEmpty() ? null : personList.get(0);
    }

    public Collection<Person> getPersonFriends(Person person) {
        final Query query = entityManager.createQuery(
                "select p from Person p where p " +
                        "in( select r.target from Relation r where r.owner = :person and r.friend = true ) ");
        query.setParameter("person", person);
        return query.getResultList();
    }

    public Long getPersonFriendsCount(Person person) {
        final Query query = entityManager.createQuery(
                "select count (p) from Person p where p " +
                        "in( select r.target from Relation r where r.owner = :person and r.friend = true ) ");
        query.setParameter("person", person);
        return (Long) query.getSingleResult();
    }

    public Long getPersonsNumber() {
        final Query query = entityManager.createQuery(
                "select COUNT (p) from Person p ");
        return (Long) query.getSingleResult();
    }
}
