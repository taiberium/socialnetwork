package com.getjavajob.training.web05.ahmetovr.dao.impl;

import com.getjavajob.training.web05.ahmetovr.bean.Relation;
import com.getjavajob.training.web05.ahmetovr.dao.RelationDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Taiberium on 01.10.2015.
 */
@Transactional
@Repository
public class RelationDaoImpl implements RelationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Relation save(Relation relation) {
        return entityManager.merge(relation);
    }

    @Override
    public void remove(Relation relation) {
        entityManager.remove((entityManager.contains(relation) ?
                relation : entityManager.merge(relation)));
    }

    @Override
    public Relation get(int id) {
        return entityManager.find(Relation.class, id);
    }

}
