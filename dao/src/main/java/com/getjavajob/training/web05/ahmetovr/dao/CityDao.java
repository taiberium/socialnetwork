package com.getjavajob.training.web05.ahmetovr.dao;


import com.getjavajob.training.web05.ahmetovr.bean.City;

/**
 * Created by Rustam on 04.08.2015.
 */
public interface CityDao {

    City get(int id);
}
