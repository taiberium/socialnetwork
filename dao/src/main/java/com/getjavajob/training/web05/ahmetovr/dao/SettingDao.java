package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.bean.Setting;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;

import java.util.Collection;

/**
 * Created by Rustam on 21.08.2015.
 */
public interface SettingDao {

    PersonSetting save(PersonSetting setting);

    void remove(PersonSetting setting);

    PersonSetting get(int id);

    Collection<PersonSetting> getPersonSettings(Person person);

    Setting get(SettingEnumType settingEnumType);
}
