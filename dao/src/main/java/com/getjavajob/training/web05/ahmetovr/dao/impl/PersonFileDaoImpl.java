package com.getjavajob.training.web05.ahmetovr.dao.impl;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonFile;
import com.getjavajob.training.web05.ahmetovr.dao.PersonFileDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Taiberium on 14.09.2015.
 */
@Repository
@Transactional
public class PersonFileDaoImpl implements PersonFileDao {

    @PersistenceContext
    private EntityManager entityManager;

    public PersonFile save(PersonFile personFile) {
        return entityManager.merge(personFile);
    }

    public void remove(PersonFile personFile) {
        entityManager.remove(personFile);
    }

    public PersonFile get(int id) {
        return entityManager.find(PersonFile.class, id);
    }

    public PersonFile getByOwner(Person person) {
        final TypedQuery<PersonFile> query = entityManager.createQuery(
                " select pf from PersonFile pf where pf.owner = :person", PersonFile.class);
        query.setParameter("person", person);
        List<PersonFile> personList = query.getResultList();
        return personList.isEmpty() ? null : personList.get(0);
    }

}
