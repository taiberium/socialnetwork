package com.getjavajob.training.web05.ahmetovr.dao.impl;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.bean.Setting;
import com.getjavajob.training.web05.ahmetovr.dao.SettingDao;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * Created by Rustam on 21.08.2015.
 */
@Transactional
@Repository
public class SettingDaoImpl implements SettingDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PersonSetting save(PersonSetting setting) {
        return entityManager.merge(setting);
    }

    @Override
    public void remove(PersonSetting setting) {
        entityManager.remove(setting);
    }

    @Override
    public PersonSetting get(int id) {
        return entityManager.find(PersonSetting.class, id);
    }

    @Override
    public Setting get(SettingEnumType settingEnumType) {
        final TypedQuery<Setting> query = entityManager.createQuery("select s from Setting s", Setting.class);
        for (Setting setting : query.getResultList()) {
            if (setting.getType() == settingEnumType) {
                return setting;
            }
        }
        return entityManager.merge(new Setting(settingEnumType));
    }


    @Override
    public Collection<PersonSetting> getPersonSettings(Person person) {
        final TypedQuery<PersonSetting> query = entityManager.createQuery(
                "select ps from PersonSetting ps where ps.owner.id = :personId", PersonSetting.class);
        query.setParameter("personId", person.getId());
        return query.getResultList();
    }
}
