package com.getjavajob.training.web05.ahmetovr.dao.impl;

import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.dao.CountryDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;


/**
 * Created by Rustam on 26.07.2015.
 */
@Transactional
@Repository
public class CountryDaoImpl implements CountryDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Country get(int id) {
        return entityManager.find(Country.class, id);
    }

    @Override
    public Collection<Country> getCountries() {
        final Query query = entityManager.createQuery(
                "select m from Country m ");
        return query.getResultList();
    }

}
