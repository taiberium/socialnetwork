package com.getjavajob.training.web05.ahmetovr.dao;


import com.getjavajob.training.web05.ahmetovr.bean.Country;

import java.util.Collection;

/**
 * Created by Rustam on 04.08.2015.
 */
public interface CountryDao {

    Country get(int id);

    Collection<Country> getCountries();

}
