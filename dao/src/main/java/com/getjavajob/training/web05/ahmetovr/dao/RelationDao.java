package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Relation;

/**
 * Created by Taiberium on 01.10.2015.
 */
public interface RelationDao {

    Relation save(Relation relation);

    void remove(Relation relation);

    Relation get(int id);

}
