package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.WallRecord;

import java.util.Collection;

/**
 * Created by Rustam on 09.08.2015.
 */
public interface WallDao {

    WallRecord save(WallRecord wallRecord);

    void remove(WallRecord wallRecord);

    WallRecord get(int id);

    Collection<WallRecord> getPersonWall(Person person);
}
