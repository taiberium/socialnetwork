package com.getjavajob.training.web05.ahmetovr.dao;

import com.getjavajob.training.web05.ahmetovr.SearchFilter;
import com.getjavajob.training.web05.ahmetovr.bean.Person;

import java.util.Collection;

/**
 * Created by Rustam on 27.07.2015.
 */
public interface PersonDao {

    Person save(Person person);

    void remove(Person person);

    Person get(int id);

    Collection<Person> getPersonFriends(Person person);

    Person getPersonByEmail(String string);

    Long getPersonsNumber();

    Collection<Person> findPersons(SearchFilter filter);

    Long getPersonFriendsCount(Person person);

    boolean isFriends(Person firstPerson, Person secondPerson);

}
