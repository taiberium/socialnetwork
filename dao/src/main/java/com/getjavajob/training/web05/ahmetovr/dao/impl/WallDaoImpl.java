package com.getjavajob.training.web05.ahmetovr.dao.impl;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.WallRecord;
import com.getjavajob.training.web05.ahmetovr.dao.WallDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;


/**
 * Created by Rustam on 09.08.2015.
 */
@Transactional
@Repository
public class WallDaoImpl implements WallDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public WallRecord save(WallRecord wallRecord) {
        WallRecord dbWallRecord = entityManager.merge(wallRecord);
        return dbWallRecord;
    }

    @Override
    public void remove(WallRecord wallRecord) {
        entityManager.remove(wallRecord);
    }

    @Override
    public WallRecord get(int id) {
        return entityManager.find(WallRecord.class, id);
    }

    @Override
    public Collection<WallRecord> getPersonWall(Person person) {
        final TypedQuery<WallRecord> query = entityManager.createQuery(
                "select w from WallRecord w where w.owner.id = :personId ORDER BY w.timestamp DESC", WallRecord.class);
        query.setParameter("personId", person.getId());
        return query.getResultList();
    }
}
