package com.getjavajob.training.web05.ahmetovr.dao.impl;

import com.getjavajob.training.web05.ahmetovr.bean.Message;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.dao.MessageDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Collection;

/**
 * Created by Rustam on 20.08.2015.
 */
@Transactional
@Repository
public class MessageDaoImpl implements MessageDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Message save(Message message) {
        return entityManager.merge(message);
    }

    @Override
    public void remove(Message message) {
        entityManager.remove(message);
    }

    @Override
    public Message get(int id) {
        return entityManager.find(Message.class, id);
    }

    @Override
    public Collection<Message> getMessagesByPersons(Person firstPerson, Person secondPerson) {
        final TypedQuery<Message> query = entityManager.createQuery(
                "select m from Message m where (m.owner = :firstPerson and m.author = :secondPerson)" +
                        "or (m.owner = :secondPerson and m.author = :firstPerson) ORDER BY m.time DESC", Message.class);
        query.setParameter("firstPerson", firstPerson);
        query.setParameter("secondPerson", secondPerson);
        return query.getResultList();
    }

}
