# Social Network  
Social network for communicating with friends!  
  
** Functionality: **  
+ authentication;  
+ registration;    
+ search with filter;  
+ profile modification;  
+ display profile;  
+ privacy settings;  
+ message communication;  
+ upload avatar;    

** Screenshots: **  
Start page:
[![https://gyazo.com/330bd9a484c11105cdc05518814b0eeb](https://i.gyazo.com/330bd9a484c11105cdc05518814b0eeb.png)](https://gyazo.com/330bd9a484c11105cdc05518814b0eeb)  

Login page:
[![https://gyazo.com/d5e6e5326ddad02a33cf400d0cc71dde](https://i.gyazo.com/d5e6e5326ddad02a33cf400d0cc71dde.png)](https://gyazo.com/d5e6e5326ddad02a33cf400d0cc71dde)  

Registration page:
[![https://gyazo.com/6438fd4895678d32e0e83818a11de006](https://i.gyazo.com/6438fd4895678d32e0e83818a11de006.png)](https://gyazo.com/6438fd4895678d32e0e83818a11de006)  

Main page:
[![https://gyazo.com/7f85c4fe45f43bec7082b7f2f8f9ddbc](https://i.gyazo.com/7f85c4fe45f43bec7082b7f2f8f9ddbc.png)](https://gyazo.com/7f85c4fe45f43bec7082b7f2f8f9ddbc)  

Privacy settings page:
[![https://gyazo.com/2df4915ddcecb8f5041eacbb4a20bc6c](https://i.gyazo.com/2df4915ddcecb8f5041eacbb4a20bc6c.png)](https://gyazo.com/2df4915ddcecb8f5041eacbb4a20bc6c) 

Search page:
[![https://gyazo.com/1b29b86692a64f872e4303055fa60b42](https://i.gyazo.com/1b29b86692a64f872e4303055fa60b42.png)](https://gyazo.com/1b29b86692a64f872e4303055fa60b42)  

Help page:
[![https://gyazo.com/20c1e025a9213479fb082141e9047b15](https://i.gyazo.com/20c1e025a9213479fb082141e9047b15.png)](https://gyazo.com/20c1e025a9213479fb082141e9047b15)



** Tools: **    
JDK 7, Spring 4, JPA / Hibernate 5, JUnit 4.12, Mockito 2.0.31, Jackson, JSP, jQuery, Twitter Bootstrap, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 14.  

** Notes: **   
SQL ddl is located in the `dao/src/main/resources/sqlscripts/create_data_model.sql`  

**Ахметов Рустам**  
===========================
Java Web <05> <2015> <05>
([www.getjavajob.com](http://www.getjavajob.com))
----------------------------