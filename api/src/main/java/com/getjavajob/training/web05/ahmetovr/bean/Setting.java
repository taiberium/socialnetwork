package com.getjavajob.training.web05.ahmetovr.bean;

import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Rustam on 21.08.2015.
 */
@Entity
@Table(name = "setting")
public class Setting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "name")
    private SettingEnumType type;

    /**
     * Think twice before use this constructor, better use get from settingService
     */
    @Deprecated
    public Setting(SettingEnumType type) {
        this.type = type;
    }

    public Setting() {
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SettingEnumType getType() {
        return type;
    }

    public void setType(SettingEnumType name) {
        this.type = name;
    }
}
