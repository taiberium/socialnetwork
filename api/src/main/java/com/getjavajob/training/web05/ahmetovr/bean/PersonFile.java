package com.getjavajob.training.web05.ahmetovr.bean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Taiberium on 14.09.2015.
 */
@Entity
@Table(name = "person_file")
public class PersonFile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @OneToOne
    private Person owner;

    @NotNull
    private String name;

    @NotNull
    @Column(name = "data")
    private byte[] data;

    public PersonFile(Person owner, String name, byte[] data) {
        this.owner = owner;
        this.name = name;
        this.data = data;
    }

    public PersonFile() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonFile that = (PersonFile) o;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode() {
        int result = owner != null ? owner.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
