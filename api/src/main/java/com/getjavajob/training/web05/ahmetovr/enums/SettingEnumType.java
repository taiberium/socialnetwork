package com.getjavajob.training.web05.ahmetovr.enums;

/**
 * Created by Rustam on 21.08.2015.
 */
public enum SettingEnumType {
    PROFILE_VIEW, FRIENDS_VIEW, WALL_WRITE, AGE_VIEW;

    public static SettingEnumValue getDefaultValue(SettingEnumType enumType) {
        return enumType == WALL_WRITE ?
                SettingEnumValue.REGGED : SettingEnumValue.ALL;
    }
}
