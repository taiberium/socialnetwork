package com.getjavajob.training.web05.ahmetovr.bean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Rustam on 20.08.2015.
 */

@Entity
@Table(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "body")
    private String text;

    @NotNull
    @Column(name = "time")
    private Timestamp time = new Timestamp(new Date().getTime());

    @ManyToOne
    @NotNull
    @JoinColumn(name = "author_person_id", referencedColumnName = "id")
    private Person author;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    private Person owner;

    @Column(name = "read")
    private boolean read = false;

    public Message(String text, Person author, Person owner) {
        this.text = text;
        this.author = author;
        this.owner = owner;
    }

    public Message() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Person getAuthor() {
        return author;
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
