package com.getjavajob.training.web05.ahmetovr.bean;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by Rustam on 24.07.2015.
 */
@Entity
@Table(name = "person")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "owner")
    /*@JoinColumn(name = "person_id")*/
    @JsonManagedReference
    private Set<Relation> relations;

    private String gender;

    @NotNull(message = "email can't be null")
    private String email;

    private Date birth;

    @NotNull(message = "password can't be null")
    private String password;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "location")
    private City location;

    public Person() {
    }

    public Person(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Person(int id, String email, String password) {
        this(email, password);
        this.id = id;
    }

    public Person(String email, String password, String firstName, String lastName) {
        this(email, password);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(int id, String email, String password, String firstName, String lastName) {
        this(email, password, firstName, lastName);
        this.id = id;
    }

    //save copy
    public Person(Person originalPerson) {
        this.id = originalPerson.id;
        this.firstName = originalPerson.firstName;
        this.lastName = originalPerson.lastName;
        this.birth = originalPerson.birth;
        this.gender = originalPerson.gender;
        this.location = originalPerson.location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Relation> getRelations() {
        return relations;
    }

    public void setRelations(Set<Relation> relations) {
        this.relations = relations;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public City getLocation() {
        return location;
    }

    public void setLocation(City location) {
        this.location = location;
    }

    public String getFullName() {
        String fName = firstName != null ? firstName : "";
        String lName = lastName != null ? lastName : "";
        return fName + " " + lName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        if (gender != null ? !gender.equals(person.gender) : person.gender != null) return false;
        if (email != null ? !email.equals(person.email) : person.email != null) return false;
        return !(password != null ? !password.equals(person.password) : person.password != null);

    }

    @Override
    public int hashCode() {
        int result = gender != null ? gender.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

}
