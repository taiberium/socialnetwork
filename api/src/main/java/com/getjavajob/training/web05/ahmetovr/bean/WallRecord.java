package com.getjavajob.training.web05.ahmetovr.bean;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Rustam on 09.08.2015.
 */
@Entity
@Table(name = "wall")
public class WallRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String body;

    @NotNull
    @Column(name = "time")
    private Timestamp timestamp = new Timestamp(new Date().getTime());

    @NotNull
    @ManyToOne
    @JoinColumn(name = "author_person_id")
    private Person author;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person owner;

    public WallRecord(String body, Person author, Person owner) {
        this.body = body;
        this.author = author;
        this.owner = owner;
    }

    public WallRecord() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Person getAuthor() {
        return author;
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WallRecord that = (WallRecord) o;
        if (!body.equals(that.body)) return false;
        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
        if (!author.equals(that.author)) return false;
        return owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        int result = body.hashCode();
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + author.hashCode();
        result = 31 * result + owner.hashCode();
        return result;
    }
}
