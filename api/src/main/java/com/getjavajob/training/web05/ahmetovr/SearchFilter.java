package com.getjavajob.training.web05.ahmetovr;

import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.bean.Person;

/**
 * Created by Rustam on 24.08.2015.
 */
public class SearchFilter {

    private Person person;
    private City city;
    private String name;
    private String lastName;
    private int age;
    private int ageDifference;
    private String sex;
    private Country country;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public int getAgeDifference() {
        return ageDifference;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAge(int age, int ageDifference) {
        this.age = age;
        this.ageDifference = ageDifference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchFilter that = (SearchFilter) o;
        if (age != that.age) return false;
        if (ageDifference != that.ageDifference) return false;
        if (person != null ? !person.equals(that.person) : that.person != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (sex != null ? !sex.equals(that.sex) : that.sex != null) return false;
        return !(country != null ? !country.equals(that.country) : that.country != null);
    }

    @Override
    public int hashCode() {
        int result = person != null ? person.hashCode() : 0;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + ageDifference;
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        return result;
    }
}
