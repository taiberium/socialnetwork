package com.getjavajob.training.web05.ahmetovr.bean;

import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Rustam on 26.08.2015.
 */
@Entity
@Table(name = "person_setting")
public class PersonSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "person_id")
    private Person owner;

    @ManyToOne
    @NotNull
    private Setting setting;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SettingEnumValue val;

    public PersonSetting(Person owner, Setting setting, SettingEnumValue val) {
        this.owner = owner;
        this.setting = setting;
        this.val = val;
    }

    public PersonSetting(Person owner, SettingEnumType settingType, SettingEnumValue val) {
        this(owner, new Setting(settingType), val);
    }

    public PersonSetting() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public SettingEnumValue getVal() {
        return val;
    }

    public void setVal(SettingEnumValue val) {
        this.val = val;
    }
}
