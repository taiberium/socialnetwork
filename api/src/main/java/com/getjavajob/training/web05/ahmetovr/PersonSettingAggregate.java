package com.getjavajob.training.web05.ahmetovr;

import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;

/**
 * Created by Rustam on 08.09.2015.
 */
public class PersonSettingAggregate {

    PersonSetting profileView;
    PersonSetting friendsView;
    PersonSetting ageView;
    PersonSetting wallWrite;

    public PersonSetting getProfileView() {
        return profileView;
    }

    public void setProfileView(PersonSetting profileView) {
        this.profileView = profileView;
    }

    public PersonSetting getFriendsView() {
        return friendsView;
    }

    public void setFriendsView(PersonSetting friendsView) {
        this.friendsView = friendsView;
    }

    public PersonSetting getAgeView() {
        return ageView;
    }

    public void setAgeView(PersonSetting ageView) {
        this.ageView = ageView;
    }

    public PersonSetting getWallWrite() {
        return wallWrite;
    }

    public void setWallWrite(PersonSetting wallWrite) {
        this.wallWrite = wallWrite;
    }
}
