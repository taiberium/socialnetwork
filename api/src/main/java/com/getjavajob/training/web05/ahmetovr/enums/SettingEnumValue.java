package com.getjavajob.training.web05.ahmetovr.enums;

/**
 * Created by Rustam on 26.08.2015.
 */
public enum SettingEnumValue {
    ALL, REGGED, FRIENDS, NONE;

    public static SettingEnumValue getSettingValue(String val) {
        if (val == null) {
            return null;
        }
        switch (val) {
            case "ALL":
                return ALL;
            case "REGGED":
                return REGGED;
            case "FRIENDS":
                return FRIENDS;
            case "NONE":
                return NONE;
            default:
                return null;
        }
    }
}
