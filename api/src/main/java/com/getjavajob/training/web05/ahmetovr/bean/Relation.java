package com.getjavajob.training.web05.ahmetovr.bean;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Taiberium on 10.09.2015.
 */
@Entity
@Table(name = "relationship")
public class Relation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "person_id")
    @JsonBackReference
    private Person owner;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "opponent_id")
    @JsonBackReference
    private Person target;

    @NotNull
    private boolean friend;

    public Relation(Person owner, Person target, boolean friend) {
        this.owner = owner;
        this.target = target;
        this.friend = friend;
    }

    public Relation() {
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getTarget() {
        return target;
    }

    public void setTarget(Person target) {
        this.target = target;
    }

    public boolean isFriend() {
        return friend;
    }

    public void setFriend(boolean friend) {
        this.friend = friend;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Relation relation = (Relation) o;
        if (friend != relation.friend) return false;
        if (owner != null ? !owner.equals(relation.owner) : relation.owner != null) return false;
        return !(target != null ? !target.equals(relation.target) : relation.target != null);

    }

    @Override
    public int hashCode() {
        int result = owner != null ? owner.hashCode() : 0;
        result = 31 * result + (target != null ? target.hashCode() : 0);
        result = 31 * result + (friend ? 1 : 0);
        return result;
    }
}
