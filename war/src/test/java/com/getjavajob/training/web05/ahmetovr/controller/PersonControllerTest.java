package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue;
import com.getjavajob.training.web05.ahmetovr.filters.ReggedFilter;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Taiberium on 03.10.2015.
 */
public class PersonControllerTest {

    private MockMvc mockMvc;
    private PersonService personService;
    private SettingService settingService;
    private CurrentUser currentUser;
    private LocationService locationService;

    @Before
    public void setup() throws Exception {
        //mock config
        locationService = mock(LocationService.class);
        personService = mock(PersonService.class);
        settingService = mock(SettingService.class);
        currentUser = mock(CurrentUser.class);

        //view resolver config
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/");
        viewResolver.setSuffix(".jsp");

        // filter and currentUser config
        currentUser = new CurrentUser();
        currentUser.setPerson(new Person(100, "test@mail.ru", "233", "Leo", "Tolstoy"));
        ReggedFilter filter = new ReggedFilter();
        filter.setUser(currentUser);

        //mockMvc config
        this.mockMvc = standaloneSetup(new PersonController(personService, settingService,
                currentUser, locationService))
                .setViewResolvers(viewResolver)
                .addFilter(filter, "/regged/*")
                .build();
    }

    @Test
    public void getFriends_forwards_friends() throws Exception {
        Person leo = currentUser.getPerson();
        when(personService.getPersonById(leo.getId())).thenReturn(leo);
        when(personService.getPersonFriendsCount(leo)).thenReturn(20L);
        Collection<Country> countries = asList(new Country(20, "England"), new Country(50, "Russia"));
        when(locationService.getAllCountries()).thenReturn(countries);

        this.mockMvc.perform(
                get("/friends/" + leo.getId())
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("viewedPerson", leo))
                .andExpect(model().attribute("friendsCount", 20L))
                .andExpect(model().attribute("countries", countries))
                .andExpect(model().attribute("regged", true))
                .andExpect(model().attribute("path", "friends.jsp"))
                .andExpect(view().name("mainPage")).andReturn();
    }

    @Test
    public void getPersonConfig_forwards_personConfig() throws Exception {
        Collection<Country> countries = asList(new Country(20, "England"), new Country(50, "Russia"));
        when(locationService.getAllCountries()).thenReturn(countries);

        this.mockMvc.perform(
                get("/regged/personConfig")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("countries", countries))
                .andExpect(model().attribute("regged", true))
                .andExpect(model().attribute("path", "regged/personConfig.jsp"))
                .andExpect(view().name("mainPage")).andReturn();
    }

    @Test
    public void postPersonConfig_forwards_getPersonConfig() throws Exception {
        this.mockMvc.perform(
                post("/regged/personConfig")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/regged/personConfig")).andReturn();
    }

    @Test
    public void getSettings_forwards_settings() throws Exception {
        Person leo = currentUser.getPerson();
        when(settingService.getPersonSettings(leo)).thenReturn(new PersonSettingAggregate());

        this.mockMvc.perform(
                get("/regged/settings")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("profileViewVal", SettingEnumValue.ALL))
                .andExpect(model().attribute("friendsViewVal", SettingEnumValue.ALL))
                .andExpect(model().attribute("ageViewVal", SettingEnumValue.ALL))
                .andExpect(model().attribute("wallWriteVal", SettingEnumValue.REGGED))
                .andExpect(model().attribute("regged", true))
                .andExpect(model().attribute("path", "regged/settings.jsp"))
                .andExpect(view().name("mainPage")).andReturn();
    }

    //TODO Here i stepan show how test SAVE
    @Test
    public void postSettings_redirect_getSettings() throws Exception {
        Person leo = currentUser.getPerson();
        when(settingService.getPersonSettings(leo)).thenReturn(new PersonSettingAggregate());
        doAnswer(new Answer() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {

                return null;
            }
        }).when(settingService).save(any(PersonSetting.class));
        this.mockMvc.perform(
                post("/regged/settings")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/regged/settings")).andReturn();
    }

    @Test
    public void isOnline_redirect_getSettings() throws Exception {
        Person leo = currentUser.getPerson();
        CurrentUser.setOnline(leo);
        when(personService.getPersonById(leo.getId())).thenReturn(leo);

        this.mockMvc.perform(
                get("/statusDisplay/" + leo.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(true)))
                .andReturn();
    }
}
