package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.filters.ReggedFilter;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.Collection;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Taiberium on 03.10.2015.
 */
public class AuthorisationControllerTest {

    private MockMvc mockMvc;
    private PersonService personService;
    private LocationService locationService;

    @Before
    public void setup() throws Exception {
        personService = mock(PersonService.class);
        locationService = mock(LocationService.class);
        CurrentUser currentUser = mock(CurrentUser.class);

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/");
        viewResolver.setSuffix(".jsp");

        this.mockMvc = standaloneSetup(new AuthorisationController(personService, locationService, currentUser))
                .setViewResolvers(viewResolver)
                .addFilter(new ReggedFilter(), "/regged/*")
                .build();
    }

    @Test
    public void getLogin_forwards_to_login() throws Exception {
        this.mockMvc.perform(
                get("/login")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", "login.jsp"))
                .andExpect(model().attribute("regged", false))

                .andExpect(view().name("mainPage")).andReturn();
    }

    @Test
    public void postLogin_forwards_to_login() throws Exception {
        this.mockMvc.perform(
                post("/login")
                        .param("email", "wrong_email")
                        .param("password", "wrong_password")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", "login.jsp"))
                .andExpect(model().attribute("regged", false))
                .andExpect(model().attribute("wrongUser", "no such user or wrong password"))
                .andExpect(view().name("mainPage")).andReturn();
    }

    @Test
    public void postLogin_forwards_to_homePage() throws Exception {
        //prepare mock
        String correctPassword = "password";
        String correctEmail = "email@mail.ru";
        when(personService.getPersonByEmail(correctEmail))
                .thenReturn(new Person(100, correctEmail, correctPassword));

        //get mvc result
        this.mockMvc.perform(
                post("/login")
                        .param("email", correctEmail)
                        .param("password", correctPassword)
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/home/100")).andReturn();
    }

    @Test
    public void getRegistration_forwards_to_personConfig() throws Exception {
        Country russia = new Country("Mother Russia!");
        when(locationService.getAllCountries())
                .thenReturn(asList(russia));

        final MvcResult mvcResult = this.mockMvc.perform(
                get("/registration")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", "registration.jsp"))
                .andExpect(model().attribute("regged", false))
                .andExpect(model().attribute("registration", "registration"))
                .andExpect(model().attribute("countries", instanceOf(Collection.class)))
                .andExpect(view().name("mainPage")).andReturn();

        final Map<String, Object> resultModelMap = mvcResult.getModelAndView().getModel();
        final Collection<Country> countries = (Collection) resultModelMap.get("countries");

        assertTrue(countries.contains(russia));
    }

    @Test
    public void registration_uses_personConfigHelper() throws Exception {
        this.mockMvc.perform(
                post("/registration")
                        .param("registration", "registration")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", "registration.jsp"))
                .andExpect(view().name("mainPage")).andReturn();
    }

}
