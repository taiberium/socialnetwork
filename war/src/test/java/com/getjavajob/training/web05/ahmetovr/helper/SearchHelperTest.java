package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.SearchFilter;
import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import org.junit.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.SearchHelper.getSavePersonCollection;
import static com.getjavajob.training.web05.ahmetovr.helper.SearchHelper.getSearchFilterBuilder;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Taiberium on 16.10.2015.
 */
public class SearchHelperTest {

    @Test
    public void getSavePersonCollection_Test() {
        //prepare persons
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");

        //get result
        Collection<Person> savePersonCollection = getSavePersonCollection(asList(leo, maks));

        //test result
        assertTrue("leo unsave copy have email", leo.getEmail() != null);
        assertTrue("maks unsave copy have email", maks.getEmail() != null);

        for (Person person : savePersonCollection) {
            if (person.getId() == 100) {
                assertTrue("leo save copy don't have email", person.getEmail() == null);
                assertTrue("leo save copy don't have password", person.getPassword() == null);
                assertEquals("leo save copy have first name", "Leo", person.getFirstName());
                assertEquals("leo save copy have last name", "Tolstoy", person.getLastName());
            }
            if (person.getId() == 120) {
                assertTrue("maks save copy don't have email", person.getEmail() == null);
                assertTrue("maks save copy don't have password", person.getPassword() == null);
                assertEquals("maks save copy have first name", "Maks", person.getFirstName());
                assertEquals("maks save copy have last name", "Jdodetz", person.getLastName());
            }
        }
    }

    @Test
    public void getSearchFilterBuilder_withoutParams_Test() {
        //prepare data
        Country russia = new Country(100, "Russia");
        City moscow = new City(100, "Moscow", russia);
        LocationService locationService = mock(LocationService.class);
        when(locationService.getCountry(100)).thenReturn(russia);
        when(locationService.getCity(100)).thenReturn(moscow);
        Map<String, String> params = new HashMap<>();

        //just check what there is no exceptions
        getSearchFilterBuilder(params, locationService);
    }

    @Test
    public void getSearchFilterBuilder_withAllParams_Test() {
        //prepare data
        Country russia = new Country(100, "Russia");
        City moscow = new City(100, "Moscow", russia);
        LocationService locationService = mock(LocationService.class);
        when(locationService.getCountry(100)).thenReturn(russia);
        when(locationService.getCity(100)).thenReturn(moscow);

        Map<String, String> params = new HashMap<>();
        params.put("name", "John Stuart");
        params.put("ageFrom", "10");
        params.put("ageTo", "30");
        params.put("countryId", "100");
        params.put("cityId", "100");
        params.put("sex", "male");

        //get result
        SearchFilter searchFilter = getSearchFilterBuilder(params, locationService);

        //check result
        assertEquals("first name", "John", searchFilter.getName());
        assertEquals("last name", "Stuart", searchFilter.getLastName());
        assertEquals("age from", 10, searchFilter.getAge());
        assertEquals("age to", 30, searchFilter.getAgeDifference());
        assertEquals("country", russia, searchFilter.getCountry());
        assertEquals("city", moscow, searchFilter.getCity());
        assertEquals("sex", "male", searchFilter.getSex());
    }
}
