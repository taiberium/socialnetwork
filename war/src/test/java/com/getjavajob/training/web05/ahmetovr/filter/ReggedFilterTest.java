package com.getjavajob.training.web05.ahmetovr.filter;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.filters.ReggedFilter;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Taiberium on 16.10.2015.
 */
public class ReggedFilterTest {

    ReggedFilter filter;
    MockFilterChain mockChain;
    MockHttpServletRequest request;
    MockHttpServletResponse response;

    @Before
    public void setup() {
        filter = new ReggedFilter();
        mockChain = new MockFilterChain();
        request = new MockHttpServletRequest("GET", "/newUrl");
        response = new MockHttpServletResponse();
    }

    @Test
    public void filter_if_userIsNull() throws IOException, ServletException {
        //config before test
        String contextPath = "/test";
        request.setContextPath(contextPath);

        //test
        filter.doFilter(request, response, mockChain);

        //verify
        assertTrue("filter chain don't have request", mockChain.getRequest() == null);
        assertEquals("redirect if not registered user", contextPath + "/login", response.getRedirectedUrl());
    }

    @Test
    public void filter_if_userIsNotNull() throws IOException, ServletException {
        //config before test
        CurrentUser user = new CurrentUser();
        user.setPerson(new Person());
        filter.setUser(user);

        //test
        filter.doFilter(request, response, mockChain);

        //filter chain have request
        assertEquals("filter chain have request", "/newUrl",
                ((HttpServletRequest) mockChain.getRequest()).getRequestURI());
    }
}
