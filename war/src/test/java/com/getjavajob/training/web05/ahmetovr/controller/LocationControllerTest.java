package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Taiberium on 03.10.2015.
 */
public class LocationControllerTest {

    private MockMvc mockMvc;
    private LocationService locationService;

    @Before
    public void setup() throws Exception {
        locationService = mock(LocationService.class);

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/");
        viewResolver.setSuffix(".jsp");

        this.mockMvc = standaloneSetup(new LocationController(locationService))
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void getCity_returns_json() throws Exception {
        Country russia = new Country(100, "Russia");
        russia.setCities(asList(new City("Moscow"), new City("Ufa")));
        when(locationService.getCountry(100)).thenReturn(russia);

        this.mockMvc.perform(
                get("/cityDisplay/100")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("Moscow")))
                .andExpect(jsonPath("$[1].name", is("Ufa")))
                .andReturn();
    }
}
