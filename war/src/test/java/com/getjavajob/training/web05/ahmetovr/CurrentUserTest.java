package com.getjavajob.training.web05.ahmetovr;

import com.getjavajob.training.web05.ahmetovr.bean.Person;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Taiberium on 16.10.2015.
 */
public class CurrentUserTest {

    @Test
    public void setOnline() {
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");

        assertFalse("isOnline before setting online", CurrentUser.isOnline(leo));
        CurrentUser.setOnline(leo);
        assertTrue("isOnline after setting online", CurrentUser.isOnline(leo));
    }

    @Test
    public void resetOnline() {
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");

        CurrentUser.setOnline(leo);
        assertTrue("isOnline before reset", CurrentUser.isOnline(leo));
        CurrentUser.resetOnline(leo);
        assertFalse("isOnline after reset", CurrentUser.isOnline(leo));
    }

    @Test
    public void setPerson() {
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        CurrentUser currentUser = new CurrentUser();

        assertTrue("getPerson is null before set", currentUser.getPerson() == null);
        currentUser.setPerson(leo);
        assertEquals("getPerson is not null after set", leo, currentUser.getPerson());
    }
}
