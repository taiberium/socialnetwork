package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import static com.getjavajob.training.web05.ahmetovr.helper.RedirectHelper.redirectWithLogin;
import static org.junit.Assert.assertEquals;

/**
 * Created by Taiberium on 16.10.2015.
 */
public class RedirectHelperTest {

    @Test
    public void redirectWithLoginTest() {
        //prepare data
        Person leo = new Person(100, "test@mail.ru", "233");
        CurrentUser currentUser = new CurrentUser();

        //get result
        ModelAndView modelAndView = redirectWithLogin(leo, currentUser);

        //test result
        assertEquals("person is setted online", true, CurrentUser.isOnline(leo));
        assertEquals("person is current user now", leo, currentUser.getPerson());
        assertEquals("view of ModelAndView", "redirect:/home/" + leo.getId(), modelAndView.getViewName());
    }
}
