package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.filters.ReggedFilter;
import com.getjavajob.training.web05.ahmetovr.service.PersonFileService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Taiberium on 03.10.2015.
 */
public class FileControllerTest {

    private MockMvc mockMvc;
    private PersonFileService personFileService;
    private PersonService personService;

    @Before
    public void setup() throws Exception {
        personService = mock(PersonService.class);
        personFileService = mock(PersonFileService.class);

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/");
        viewResolver.setSuffix(".jsp");

        // filter config
        CurrentUser currentUser = new CurrentUser();
        currentUser.setPerson(new Person(100, "test@mail.ru", "233", "Max", "Johnson"));
        ReggedFilter filter = new ReggedFilter();
        filter.setUser(currentUser);

        this.mockMvc = standaloneSetup(new FileController(personFileService, currentUser, personService))
                .setViewResolvers(viewResolver)
                .addFilter(filter, "/regged/*")
                .build();
    }

    @Test
    public void getImageConfig_forwards_to_photoView() throws Exception {
        this.mockMvc.perform(
                get("/regged/photo")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", "regged/photoView.jsp"))
                .andExpect(model().attribute("regged", true))
                .andExpect(view().name("mainPage")).andReturn();
    }

    //TODO multipart file!

    @Test
    public void postImageConfig_forwards_to_photoView() throws Exception {
        MockMultipartFile aPic = new MockMultipartFile("avatar.jpg", new byte[]{1, 2, 34, 5});
        this.mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/regged/photo")
                        .file(aPic)
                        .header("content-type", "multipart/form-data")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", "regged/photoView.jsp"))
                .andExpect(model().attribute("regged", true))
                .andExpect(view().name("mainPage")).andReturn();
    }
}
