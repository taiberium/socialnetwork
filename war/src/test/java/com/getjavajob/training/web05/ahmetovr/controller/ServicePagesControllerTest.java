package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Taiberium on 03.10.2015.
 */
public class ServicePagesControllerTest {

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        CurrentUser currentUser = mock(CurrentUser.class);

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/");
        viewResolver.setSuffix(".jsp");

        this.mockMvc = standaloneSetup(new ServicePagesController(currentUser))
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void getStartPage_returns_startPage() throws Exception {
        getPageTest("/", "startPage.jsp");
    }

    @Test
    public void getHelpPage_returns_helpPage() throws Exception {
        getPageTest("/help", "help.jsp");
    }

    @Test
    public void getInfoPage_returns_infoPage() throws Exception {
        getPageTest("/info", "info.jsp");
    }

    @Test
    public void getPAgeNotFound_returns_pageNotFound() throws Exception {
        getPageTest("/pageNotFound", "pageNotFound.jsp");
    }

    private void getPageTest(String url, String path) throws Exception {
        this.mockMvc.perform(
                get(url)
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", path))
                .andExpect(model().attribute("regged", false))
                .andExpect(view().name("mainPage")).andReturn();
    }
}
