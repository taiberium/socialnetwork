package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Taiberium on 16.10.2015.
 */
public class SettingHelperTest {

    private SettingService settingService;
    private CurrentUser currentUser;
    private PersonService personService;
    private SettingsHelper settingsHelper;

    @Before
    public void setup() {
        settingService = mock(SettingService.class);
        personService = mock(PersonService.class);
        currentUser = new CurrentUser();
        settingsHelper = new SettingsHelper(settingService, currentUser, personService);
    }

    @Test
    public void setSettings_set_defaultValue() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        when(settingService.getPersonSettings(leo)).thenReturn(new PersonSettingAggregate());
        ModelAndView modelAndView = new ModelAndView();

        //make result
        settingsHelper.setSettings(modelAndView, leo);

        //check result
        assertEquals("profileView", true, modelAndView.getModel().get("profileView"));
        assertEquals("friendsView", true, modelAndView.getModel().get("friendsView"));
        assertEquals("ageView", true, modelAndView.getModel().get("ageView"));
        assertEquals("wallWrite", false, modelAndView.getModel().get("wallWrite"));

    }

    @Test
    public void setSettings_set_personSetting_value() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        PersonSettingAggregate personSetting = new PersonSettingAggregate();
        personSetting.setProfileView(new PersonSetting(leo, SettingEnumType.PROFILE_VIEW, SettingEnumValue.ALL));
        personSetting.setAgeView(new PersonSetting(leo, SettingEnumType.AGE_VIEW, SettingEnumValue.REGGED));
        personSetting.setFriendsView(new PersonSetting(leo, SettingEnumType.FRIENDS_VIEW, SettingEnumValue.FRIENDS));
        personSetting.setWallWrite(new PersonSetting(leo, SettingEnumType.WALL_WRITE, SettingEnumValue.NONE));
        when(settingService.getPersonSettings(leo)).thenReturn(personSetting);
        ModelAndView modelAndView = new ModelAndView();

        //make result
        settingsHelper.setSettings(modelAndView, leo);

        //check result
        assertEquals("profileView", true, modelAndView.getModel().get("profileView"));
        assertEquals("friendsView", false, modelAndView.getModel().get("friendsView"));
        assertEquals("ageView", false, modelAndView.getModel().get("ageView"));
        assertEquals("wallWrite", false, modelAndView.getModel().get("wallWrite"));
    }

    @Test
    public void isVisible_return_defaultVal() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");

        //get result
        boolean result = settingsHelper.isVisible(leo, null, SettingEnumValue.ALL);

        //check result
        assertTrue("default is true", result);
    }

    @Test
    public void isVisible_return_reggedVal_if_reggedViewer() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");

        currentUser.setPerson(maks);
        boolean reggedResult = settingsHelper.isVisible(leo, null, SettingEnumValue.REGGED);
        assertTrue("true if regged viewer", reggedResult);
    }

    @Test
    public void isVisible_return_reggedVal_if_unreggedViewer() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");

        boolean unreggedResult = settingsHelper.isVisible(leo, null, SettingEnumValue.REGGED);
        assertFalse("false if unregged viewer", unreggedResult);
    }

    @Test
    public void isVisible_return_friendsVal_if_friends() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");
        currentUser.setPerson(maks);
        when(personService.isFriends(leo, maks)).thenReturn(true);

        boolean result = settingsHelper.isVisible(leo, null, SettingEnumValue.FRIENDS);

        assertTrue("true if friends", result);
    }

    @Test
    public void isVisible_return_friendsVal_if_notFriends() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");
        currentUser.setPerson(maks);
        when(personService.isFriends(leo, maks)).thenReturn(false);
        //result test
        boolean result = settingsHelper.isVisible(leo, null, SettingEnumValue.FRIENDS);
        assertFalse("true if friends", result);
    }

    @Test
    public void isVisible_return_friendsVal_if_lookOnThemself() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        currentUser.setPerson(leo);
        when(personService.isFriends(leo, leo)).thenReturn(false);
        //result test
        boolean result = settingsHelper.isVisible(leo, null, SettingEnumValue.FRIENDS);
        assertTrue("true if look on themself", result);
    }

    @Test
    public void isVisible_return_noneVal_if_lookOnThemself() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        currentUser.setPerson(leo);
        when(personService.isFriends(leo, leo)).thenReturn(false);
        //result test
        boolean result = settingsHelper.isVisible(leo, null, SettingEnumValue.NONE);
        assertTrue("true if look on themself", result);
    }

    @Test
    public void isVisible_return_noneVal_if_lookOnSomeoneElse() {
        //prepare
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");
        currentUser.setPerson(maks);
        when(personService.isFriends(leo, maks)).thenReturn(false);
        //result test
        boolean result = settingsHelper.isVisible(leo, null, SettingEnumValue.NONE);
        assertFalse("false if look on someone else", result);
    }
}
