package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.SearchHelper.getSearchFilterBuilder;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Taiberium on 03.10.2015.
 */
public class SearchControllerTest {

    private MockMvc mockMvc;
    private PersonService personService;
    private LocationService locationService;

    @Before
    public void setup() throws Exception {
        locationService = mock(LocationService.class);
        personService = mock(PersonService.class);

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/");
        viewResolver.setSuffix(".jsp");

        this.mockMvc = standaloneSetup(new SearchController(personService, locationService))
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void getScriptSearch_returns_json() throws Exception {
        Person leo = new Person(100, "leo@mail.ru", "233", "Leo", "Tolstoy");
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Stivenson");
        Map<String, String> params = new HashMap<>();
        params.put("viewedPerson", "100");
        when(personService.getPersonByFilter(getSearchFilterBuilder(params, locationService)))
                .thenReturn(asList(leo, maks));

        this.mockMvc.perform(
                get("/scriptSearch")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName", is("Leo")))
                .andExpect(jsonPath("$[1].firstName", is("Maks")))
                .andReturn();
    }

    @Test
    public void getSearch_returns_json() throws Exception {
        Country russia = new Country("Russia");
        Country usa = new Country("USA");
        when(personService.getPersonsNumber()).thenReturn(3L);
        when(locationService.getAllCountries()).thenReturn(asList(russia, usa));

        this.mockMvc.perform(
                get("/regged/search")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("path", "regged/search.jsp"))
                .andExpect(model().attribute("personsNumber", 3L))
                .andExpect(model().attribute("countries", asList(russia, usa)))
                .andExpect(view().name("mainPage")).andReturn();
    }
}
