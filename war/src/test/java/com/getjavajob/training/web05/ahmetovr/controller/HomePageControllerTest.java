package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.filters.ReggedFilter;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import com.getjavajob.training.web05.ahmetovr.service.WallService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Taiberium on 03.10.2015.
 */
public class HomePageControllerTest {

    private MockMvc mockMvc;
    private PersonService personService;
    private SettingService settingService;
    private CurrentUser currentUser;

    @Before
    public void setup() throws Exception {
        //mock config
        personService = mock(PersonService.class);
        settingService = mock(SettingService.class);
        WallService wallService = mock(WallService.class);

        //view resolver config
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/");
        viewResolver.setSuffix(".jsp");

        // filter and currentUser config
        currentUser = new CurrentUser();
        currentUser.setPerson(new Person(100, "test@mail.ru", "233", "Leo", "Tolstoy"));
        ReggedFilter filter = new ReggedFilter();
        filter.setUser(currentUser);

        //mockMvc config
        this.mockMvc = standaloneSetup(new HomePageController(personService, settingService,
                wallService, currentUser))
                .setViewResolvers(viewResolver)
                .addFilter(filter, "/regged/*")
                .build();
    }

    @Test
    public void getHomePage_looks_on_itself_forwards_to_homePage() throws Exception {
        Person leo = currentUser.getPerson();
        when(personService.getPersonById(100)).thenReturn(leo);
        when(settingService.getPersonSettings(leo)).thenReturn(new PersonSettingAggregate());

        this.mockMvc.perform(
                get("/home/100")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("privateView", true))
                .andExpect(model().attribute("isFriends", false))
                .andExpect(model().attribute("regged", true))
                .andExpect(model().attribute("path", "homePage.jsp"))
                .andExpect(view().name("mainPage")).andReturn();
    }

    @Test
    public void getHomePage_looks_on_person_forwards_to_homePage() throws Exception {
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");
        when(personService.getPersonById(120)).thenReturn(maks);
        when(settingService.getPersonSettings(maks)).thenReturn(new PersonSettingAggregate());

        this.mockMvc.perform(
                get("/home/120")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("privateView", false))
                .andExpect(model().attribute("isFriends", false))
                .andExpect(model().attribute("regged", true))
                .andExpect(model().attribute("path", "homePage.jsp"))
                .andExpect(view().name("mainPage")).andReturn();
    }

    @Test
    public void getHomePage_unregged_looks_on_person_forwards_to_homePage() throws Exception {
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");
        currentUser.setPerson(null);
        when(personService.getPersonById(120)).thenReturn(maks);
        when(settingService.getPersonSettings(maks)).thenReturn(new PersonSettingAggregate());

        this.mockMvc.perform(
                get("/home/120")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attribute("settingsView", false))
                .andExpect(model().attribute("regged", false))
                .andExpect(model().attribute("path", "homePage.jsp"))
                .andExpect(view().name("mainPage")).andReturn();
    }

    @Test
    public void postWallRecord_redirect_to_home() throws Exception {
        Person leo = currentUser.getPerson();
        when(personService.getPersonById(120)).thenReturn(leo);
        when(settingService.getPersonSettings(leo)).thenReturn(new PersonSettingAggregate());

        this.mockMvc.perform(
                post("/home")
                        .param("newWall", "message")
                        .param("personView", "120")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/home/120")).andReturn();
    }

    @Test
    public void makeFriends_redirect_to_home() throws Exception {
        Person maks = new Person(120, "maks@mail.ru", "233", "Maks", "Jdodetz");
        when(personService.getPersonById(120)).thenReturn(maks);

        this.mockMvc.perform(
                post("/regged/friendsChange")
                        .param("button", "make")
                        .param("personView", "120")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/home/120")).andReturn();
    }
}
