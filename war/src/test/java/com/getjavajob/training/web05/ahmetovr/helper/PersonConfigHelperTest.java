package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.PersonConfigHelper.config;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Taiberium on 16.10.2015.
 */
public class PersonConfigHelperTest {

    private PersonService personService;
    private CurrentUser currentUser;
    private LocationService locationService;

    @Before
    public void setup() throws Exception {
        //mock config
        locationService = mock(LocationService.class);
        personService = mock(PersonService.class);
        currentUser = new CurrentUser();
        currentUser.setPerson(new Person(100, "test@mail.ru", "233", "Leo", "Tolstoy"));
    }

    @Test
    public void config_with_registration_param_and_not_all_params() {
        currentUser.setPerson(null);
        Map<String, String> params = new HashMap<>();
        params.put("registration", "registration");

        //get result
        ModelAndView modelAndView = config(params, currentUser, personService, locationService);

        //check result
        assertEquals("path parameter", "registration.jsp", modelAndView.getModel().get("path"));
        assertEquals("message parameter", "not all parameters was set", modelAndView.getModel().get("message"));
        assertEquals("regged parameter", false, modelAndView.getModel().get("regged"));
    }

    @Test
    public void config_with_registration_param_and_alreadyExist_person() {
        Person leo = currentUser.getPerson();
        currentUser.setPerson(null);
        when(personService.getPersonByEmail(leo.getEmail())).thenReturn(leo);
        Map<String, String> params = new HashMap<>();
        params.put("registration", "registration");
        params.put("email", leo.getEmail());
        params.put("password", leo.getPassword());

        //get result
        ModelAndView modelAndView = config(params, currentUser, personService, locationService);

        //check result
        assertEquals("path parameter", "registration.jsp", modelAndView.getModel().get("path"));
        assertEquals("message parameter", " already exist! ", modelAndView.getModel().get("message"));
        assertEquals("regged parameter", false, modelAndView.getModel().get("regged"));
    }

    @Test
    public void config_with_registration_param_and_login() {
        Person leo = currentUser.getPerson();
        currentUser.setPerson(null);
        when(personService.getPersonByEmail(leo.getEmail())).thenReturn(null);
        when(personService.save(leo)).thenReturn(leo);
        Map<String, String> params = new HashMap<>();
        params.put("registration", "registration");
        params.put("email", leo.getEmail());
        params.put("password", leo.getPassword());

        //get result
        ModelAndView modelAndView = config(params, currentUser, personService, locationService);

        //check result
        assertEquals("view of ModelAndView", "redirect:/home/" + leo.getId(),
                modelAndView.getViewName());
        assertEquals("person is currentUser", leo, currentUser.getPerson());
        assertTrue("person is online", CurrentUser.isOnline(leo));
    }

    @Test
    public void config_without_registration_param_and_alreadyExist_person() {
        Person leo = currentUser.getPerson();
        when(personService.getPersonByEmail(leo.getEmail())).thenReturn(leo);
        Map<String, String> params = new HashMap<>();
        params.put("email", leo.getEmail());
        params.put("password", leo.getPassword());

        //get result
        ModelAndView modelAndView = config(params, currentUser, personService, locationService);

        //check result
        assertEquals("path parameter", "regged/personConfig.jsp", modelAndView.getModel().get("path"));
        assertEquals("message parameter", " already exist! ", modelAndView.getModel().get("message"));
        assertEquals("regged parameter", true, modelAndView.getModel().get("regged"));
    }

    @Test
    public void config_without_registration_param_and_redirect_config() {
        Person leo = currentUser.getPerson();
        Person newLeo = new Person(100, "superLeo@mail.ru", "277", "Leo", "Tolstoy");
        when(personService.getPersonByEmail(leo.getEmail())).thenReturn(null);
        when(personService.save(newLeo)).thenReturn(newLeo);
        Map<String, String> params = new HashMap<>();
        params.put("email", newLeo.getEmail());
        params.put("password", newLeo.getPassword());

        //get result
        ModelAndView modelAndView = config(params, currentUser, personService, locationService);

        //check result
        assertEquals("view of ModelAndView", "redirect:/regged/personConfig",
                modelAndView.getViewName());
        assertTrue("new person is currentUser", newLeo.equals(currentUser.getPerson()));
        assertFalse("old person not currentuser", !leo.equals(currentUser.getPerson()));
    }
}
