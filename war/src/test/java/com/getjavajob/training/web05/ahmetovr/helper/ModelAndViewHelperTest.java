package com.getjavajob.training.web05.ahmetovr.helper;

import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;
import static org.junit.Assert.assertEquals;

/**
 * Created by Taiberium on 16.10.2015.
 */
public class ModelAndViewHelperTest {

    @Test
    public void getModelAndView_returns_modelAndView_with_regged_param() {
        //get result
        ModelAndView modelAndView = getModelAndView("/test.jsp", false);

        //test result
        assertEquals("path param", "/test.jsp", modelAndView.getModel().get("path"));
        assertEquals("regged param", false, modelAndView.getModel().get("regged"));
        assertEquals("view of ModelAndView", "mainPage", modelAndView.getViewName());
    }

    @Test
    public void getModelAndView_returns_modelAndView_without_regged_param() {
        //get result
        ModelAndView modelAndView = getModelAndView("/test.jsp");

        //test result
        assertEquals("path param", "/test.jsp", modelAndView.getModel().get("path"));
        assertEquals("view of ModelAndView", "mainPage", modelAndView.getViewName());
    }
}
