<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-5 col-md-offset-1">
        <h3 align="left">Error! Page NOT FOUND!</h3>
    </div>
    <div class="col-md-4 col-md-offset-2">
        <img src="${pageContext.request.contextPath}/pictures/idea_bot.png">
    </div>
</div>