<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-5 col-md-offset-1">
        <h3 align="left">Social Robotics - this project made by young programmer Akhmetov Rustam,
            <br>just for fun, if you want contact us</h3>

        <h3 align="left">please call 8-910-003-39-50 <br>
            or by email: akhmetovrustam1990@gmail.com</h3>
    </div>

    <div class="col-md-4 col-md-offset-2">
        <img src="${pageContext.request.contextPath}/pictures/idea_bot.png">
    </div>
</div>
