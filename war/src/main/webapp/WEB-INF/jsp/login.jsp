<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-4 "><h3> It's time to start our social connection!</h3></div>
    <div class="col-md-4 " id="custom-bootstrap-window1">
        <h3 align="center">LOGIN</h3>

        <form action="${pageContext.request.contextPath}/login" method="post">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-8">
                        <label for="email">Email:</label>
                        <input name="email" placeholder="Template@gmail.com" class="form-control" id="email">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-8">
                        <label for="password">Password:</label>
                        <input type="password" name="password" placeholder="password" class="form-control"
                               id="password">
                    </div>
                </div>
            </div>
            <br>

            <p><input style="width:100px;" type="submit" alt="login" class="btn btn-custom" name="login" value="login"></p>

        </form>
        <br>${wrongUser}
    </div>

    <div class="col-md-4 ">
        <img src="${pageContext.request.contextPath}/pictures/rocket_bot.png">
    </div>
</div>