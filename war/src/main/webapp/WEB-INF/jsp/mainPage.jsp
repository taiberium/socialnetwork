<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social robotics</title>
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <!-- css -->
    <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
    <link href=" ${pageContext.request.contextPath}/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href=" ${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <%--js--%>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/js/moment.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap-datetimepicker.js"></script>
    <script src="${pageContext.request.contextPath}/js/yasn.js"></script>
    <script> context = "${pageContext.request.contextPath}";</script>
</head>

<body id="content_main">

<div class="container">
    <h1>Social Robotics</h1>

    <jsp:include page="${requestScope.regged ?
    'partition/reggedHeaderMenu.jsp'
    :'partition/unreggedHeaderMenu.jsp'}"/>

    <div style="  min-height: 650px;">
        <jsp:include page="${requestScope.path}"/>
    </div>
    <p>&nbsp;</p>
    <div class="navbar navbar-default" id="custom-bootstrap-footer">Copyright ltd <br>
        Made by Akhmetov Rustam
    </div>
</div>

</body>
</html>