<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<h3 align="center">${registration!=null ? 'Registration':'Config'}</h3>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="email">*Email:</label>
            <input name="email"
                   value="${email}"
                   placeholder="${sessionScope['scopedTarget.currentUser'].person!=null? sessionScope['scopedTarget.currentUser'].person.getEmail() : 'template@gmail.com'}"
                   class="form-control" id="email"
                   autocomplete="off"
            ${registration!=null ? 'required':'disabled'}>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="password">*Password:</label>
            <input name="password"
                   type="password"
                   value="${password}"
                   placeholder="${sessionScope['scopedTarget.currentUser'].person!=null? '***' : 'password'}"
                   class="form-control" id="password"
                   autocomplete="off"
            ${registration!=null ? 'required':''}>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="firstName">First Name:</label>
            <input name="firstName"
                   value="${firstName}"
                   id="firstName"
                   class="form-control"
                   placeholder="${sessionScope['scopedTarget.currentUser'].person!=null?
                    sessionScope['scopedTarget.currentUser'].person.getFirstName() : 'first name'}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="lastName">Last Name:</label>
            <input name="lastName"

                   value="${lastName}"
                   id="lastName"
                   class="form-control"
                   placeholder="${sessionScope['scopedTarget.currentUser'].person!=null?
                   sessionScope['scopedTarget.currentUser'].person.getLastName() : 'last name'}">
        </div>
    </div>
</div>

<div class="row">
    <div class='col-md-8'>
        <div class="form-group">
            <label for="datetimepicker1">Birth:</label>

            <div class='input-group date' id='datetimepicker1'>
                <input type='text' name="birth" class="form-control"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });
    </script>
</div>

<input type="hidden" name="registration" value="${registration}">

<div class="row">
    <div class="form-group">
        <div class="col-md-8">
            <label for="gender">Gender:</label>
            <select name="sex"
                    class="btn btn-default dropdown-toggle"
                    id="gender"
                    style="width:230px">
                <option value="empty" ${sex=='empty'?'selected':''}>...</option>
                <option value="male" ${sex=='male'?'selected':''}>male</option>
                <option value="female" ${sex=='female'?'selected':''}>female</option>
            </select>
        </div>
    </div>
</div>

<jsp:include page="locationDisplay.jsp"/>
<p>&nbsp;</p>
<p><input type="submit" style="width:100px;" alt="ok" class="btn btn-custom" name="ok" value="ok"></p>
${message}
