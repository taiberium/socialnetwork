<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form id="searchForm" action="${param.path}" method="get">
    <div class="row">
        <div class="form-group">
            <div class="col-md-10">
                <label for="name">Name:</label>
                <input style="width:230px" name="name" placeholder="Write friends name" class="form-control" id="name">
            </div>
        </div>
    </div>

    <div class="btn-group">
        <div class="radio"><label> <input type="radio" name="sex" value="male"> Male </label></div>
        <div class="radio"><label> <input type="radio" name="sex" value="female"> Female </label></div>
        <div class="radio"><label> <input type="radio" name="sex" value="any"> Any </label></div>
    </div>

    <input type="hidden" name="viewedPerson" value="${viewedPerson.getId()}">

    <jsp:include page="locationDisplay.jsp"/>

    <div class="row">
        <div class="form-group">
            <div class="col-md-4">
                <label for="age">Age From:</label>
                <input name="ageFrom" class="form-control" id="age"/>
            </div>
            <div class="col-md-4">
                <label for="age">To:</label>
                <input name="ageTo" class="form-control" id="ageBetween"/>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    <input type="button" id="searchButton" alt="search" class="btn btn-custom" name="search"
           value="search">
</form>

<br>

<div id="profiles"></div>

<script>
    $(window).load(getPersons);
    /*just prints result by any change*/
    $("input[type='checkbox'], input[type='radio'] , input[type='button']").on("click", getPersons);
    $("select").on("change", getPersons);
</script>

