<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <div class="form-group">
        <div class="col-md-8">
            <label>${param.words}</label>
            <select style="width:150px"
                    name="${param.viewSetting}"
                    class="btn btn-default dropdown-toggle">
                <c:if test="${param.viewSetting != 'wallWrite'}">
                    <option value="ALL"
                            <c:if test="${param.viewVal=='ALL'}"> selected</c:if> >All
                    </option>
                </c:if>
                <option value="REGGED"
                        <c:if test="${param.viewVal=='REGGED'}"> selected</c:if> >REGGED
                </option>
                <option value="FRIENDS"
                        <c:if test="${param.viewVal=='FRIENDS'}"> selected</c:if> >FRIENDS
                </option>
                <option value="NONE"
                        <c:if test="${param.viewVal=='NONE'}"> selected</c:if> >NONE
                </option>
            </select>
        </div>
    </div>
</div>
<p>&nbsp;</p>
