<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <div class="form-group">
        <div class="col-md-6">
            <label for="scriptCountry">Country:</label>
            <select name="countryId" id="scriptCountry" class="btn btn-default dropdown-toggle" style="width:230px"
                    onchange="getCities('scriptCountry','scriptCity')">
                <option value=""> ...</option>
                <c:forEach var="country" items="${countries}" varStatus="ctr">
                    <option value="<c:out value="${country.id}"/>">
                        <c:out value="${country.name}"/>
                    </option>
                </c:forEach>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-md-6">
            <label for="scriptCity">City:</label>
            <select name="cityId" id="scriptCity" class="btn btn-default dropdown-toggle" style="width:230px">
                <option value=""> ...</option>
            </select>
        </div>
    </div>
</div>