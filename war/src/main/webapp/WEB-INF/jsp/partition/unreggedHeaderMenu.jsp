<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="custom-bootstrap-menu" class="navbar navbar-default">
    <div class="navbar-header">

        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>

    <div class="collapse navbar-collapse navbar-menubuilder">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="${pageContext.request.contextPath}/login">Login</a></li>
            <li><a href="${pageContext.request.contextPath}/registration">Register</a></li>
            <li><a href="${pageContext.request.contextPath}/info">About us</a></li>
        </ul>
    </div>
</div>