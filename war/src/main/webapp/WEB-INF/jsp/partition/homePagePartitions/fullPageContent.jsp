<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">

    <%--profile view--%>
    <div class="col-md-3 col-md-offset-1" id="custom-bootstrap-window1">
        <h4>${fullName}</h4>

        <div id="online"></div>
        <c:if test="${privateView}"><a align="center" href="${pageContext.request.contextPath}/regged/photo"> </c:if>
        <img src="${pageContext.request.contextPath}/imageDisplay/${personView.id}" class="img-rounded" alt="photo"
             width="220" height="180"
             onerror="if (this.src != '${pageContext.request.contextPath}/pictures/user-default.jpg')
             this.src = '${pageContext.request.contextPath}/pictures/user-default.jpg';">
        <c:if test="${privateView}"> </a></c:if><br>


        <c:if test="${privateView == false}">
            <c:if test="${isFriends==true}">
                ${personView.firstName} is your friend<br>

                <form action="${pageContext.request.contextPath}/regged/friendsChange" method="post">
                    <input type="submit" class="btn btn-custom" name="button" value="remove friend">
                    <input type="hidden" name="personView" value="${personView.id}">
                </form>
            </c:if>

            <c:if test="${isFriends==false}">
                ${personView.firstName} is not your friend<br>

                <form action="${pageContext.request.contextPath}/regged/friendsChange" method="post">
                    <input type="submit" class="btn btn-custom" name="button" value="make friends">
                    <input type="hidden" name="personView" value="${personView.id}">
                </form>
            </c:if>
            <br>
        </c:if>

        <c:if test="${friendsView}"> <p class="menuText">Authority: ${friendsCount} friends<br> </c:if>

        <c:if test="${personView.gender != null}"> Gender: <c:out value="${personView.gender}"/><br> </c:if>

        <%--ageView--%>
        <c:if test="${ageView == true && personView.birth != null}">
            Age: ${age} years <br>
            Birth: <fmt:formatDate type="date" value="${personView.birth}"/> <br>
        </c:if>

        <%--Location--%>
        <c:if test="${personView.location != null}">
            Country: ${personView.location.country.name} <br>
            City: ${personView.location.name} <br>
        </c:if>
    </p><br>

        <div class="row">
            <%--friendsView--%>
            <c:if test="${friendsView}">
                <div class="col-md-3">
                    <a href="${pageContext.request.contextPath}/friends/${personView.id}"
                       class="btn btn-custom">Friends</a><br>
                </div>
            </c:if>

            <%--settingView--%>
            <c:if test="${privateView}">
                <div class="col-md-3">
                    <a href="${pageContext.request.contextPath}/regged/settings" class="btn btn-custom">Settings</a><br>
                </div>
                <div class="col-md-3">
                    <a href="${pageContext.request.contextPath}/regged/personConfig" class="btn btn-custom">Configuration</a><br>
                </div>
            </c:if>
        </div>

    </div>

    <%--the Wall column--%>
    <div class="col-md-6 col-md-offset-1" id="custom-bootstrap-window2">

        <h4> ${personView.firstName} wall </h4>

        <%--wallWrite check--%>
        <c:if test="${wallWrite}">
            <form action="${pageContext.request.contextPath}/home" method="post">

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-8">
                            <input name="newWall"
                                   placeholder="What's new?"
                                   class="form-control">
                        </div>

                        <input type="hidden" name="personView" value="${personView.id}">

                        <div class="col-md-3 col-md-offset-1">
                            <input type="submit" style="width:100px;"
                                   alt="ok" class="btn btn-custom" name="ok" value="ok">
                        </div>
                    </div>
                </div>
            </form>
        </c:if>
        <br>

        <%--wallRecord display--%>
        <c:forEach var="wallRecord" items="${wallRecordCollection}">

            <p style="background-color:#CCC;">
                <a href="${pageContext.request.contextPath}/home/${wallRecord.author.id}">
                    <img src="${pageContext.request.contextPath}/imageDisplay/${wallRecord.author.id}" alt="photo"
                         width="50" height="40"
                         style="float: left"
                         onerror="if (this.src != '${pageContext.request.contextPath}/pictures/user-default.jpg')
                      this.src = '${pageContext.request.contextPath}/pictures/user-default.jpg';">
                    <span class="wallName"> ${wallRecord.author.fullName} </span>
                </a>
                <span class="wallTime">
                    &nbsp; <fmt:formatDate type="both"
                                           dateStyle="short" timeStyle="short" value="${wallRecord.timestamp}"/>
                </span><br>
                <span class="wallComment" style="font-weight: 600;">
                        ${wallRecord.body}
                </span>
            </p>
        </c:forEach>

    </div>

</div>

<script>
    $(window).load(isOnline(${personView.id}, "online"));
    var tenSeconds = 10000;
    window.setInterval(isOnline, tenSeconds, ${personView.id}, "online");
</script>