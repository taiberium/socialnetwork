<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-4 "><h3 align="left">It's time to start our social connection!</h3></div>
    <div class="col-md-4 " id="custom-bootstrap-window1">
        <h3 align="center"> Settings </h3>

        <form action="${pageContext.request.contextPath}/regged/settings" method="post">

            <jsp:include page="../partition/settingOptionDisplay.jsp">
                <jsp:param name="words" value="Who can see Your profile:"/>
                <jsp:param name="viewSetting" value="profileView"/>
                <jsp:param name="viewVal" value="${profileViewVal}"/>
            </jsp:include>

            <jsp:include page="../partition/settingOptionDisplay.jsp">
                <jsp:param name="words" value="Who can see Your friends:"/>
                <jsp:param name="viewSetting" value="friendsView"/>
                <jsp:param name="viewVal" value="${friendsViewVal}"/>
            </jsp:include>

            <jsp:include page="../partition/settingOptionDisplay.jsp">
                <jsp:param name="words" value="Who can see Your age:"/>
                <jsp:param name="viewSetting" value="ageView"/>
                <jsp:param name="viewVal" value="${ageViewVal}"/>
            </jsp:include>

            <jsp:include page="../partition/settingOptionDisplay.jsp">
                <jsp:param name="words" value="Who can write on your wall:"/>
                <jsp:param name="viewSetting" value="wallWrite"/>
                <jsp:param name="viewVal" value="${wallWriteVal}"/>
            </jsp:include>

            <p><input type="submit" style="width:100px;"
                      alt="ok" class="btn btn-custom" name="ok" value="ok"></p>
        </form>
    </div>
    <div class="col-md-4">
        <img src="${pageContext.request.contextPath}/pictures/video_bot.png">
    </div>
</div>