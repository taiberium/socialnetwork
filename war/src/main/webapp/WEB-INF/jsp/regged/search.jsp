<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">

    <div class="col-md-3 ">
        <h3 align="left"> There is : ${personsNumber} people</h3>

        <h3 align="left">The bigger society,<br> the better future</h3>
    </div>

    <div class="col-md-5 " id="custom-bootstrap-window1">
        <h3 align="center">Search</h3>
        <jsp:include page="../partition/searchParamDisplay.jsp">
            <jsp:param name="path" value="${pageContext.request.contextPath}/regged/search"/>
        </jsp:include>
    </div>

    <div class="col-md-4 ">
        <img src="${pageContext.request.contextPath}/pictures/jobbee_bot.png">
    </div>

</div>