<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-4 "><h3> Modify Yourself!</h3></div>
    <div class="col-md-4 " id="custom-bootstrap-window1">
        <form action="${pageContext.request.contextPath}/regged/personConfig" method="post">
            <jsp:include page="../partition/personConfigDisplay.jsp"/>
        </form>
    </div>
    <div class="col-md-4">
        <img src="${pageContext.request.contextPath}/pictures/video_bot.png">
    </div>
</div>