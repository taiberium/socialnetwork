<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-4 "><h3 align="left">in developing</h3></div>
    <div class="col-md-4 " id="custom-bootstrap-window1">
        <h3 align="left">Dialogs </h3>

        <form action="${pageContext.request.contextPath}/regged/message/${opponent.id}" method="post">

            <label>
                <select name="personId"
                        class="btn btn-default dropdown-toggle" required>
                    <c:forEach var="friend" items="${friends}">
                        <option value="${friend.id}"> ${friend.fullName} </option>

                    </c:forEach>
                </select>
            </label>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-8">
                        <input name="text"
                               placeholder="Hello there!"
                               class="form-control">
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <input type="submit"
                               alt="ok" class="btn btn-custom" name="ok" value="ok">
                    </div>
                </div>
            </div>
        </form>

    </div>

    <div class="col-md-4 ">
        <img src="${pageContext.request.contextPath}/pictures/branding_bot.png">
    </div>
</div>