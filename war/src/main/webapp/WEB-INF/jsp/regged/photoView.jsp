<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-md-4 "><h3 align="left">Be neat and tidy!</h3></div>
    <div class="col-md-4 " id="custom-bootstrap-window1">
        <h3 align="center">Photo</h3>

        <p align="center">
            <img src="${pageContext.request.contextPath}/imageDisplay/${sessionScope['scopedTarget.currentUser'].person.id}"
                 class="img-rounded" alt="photo" width="220" height="180"
                 onerror="if (this.src != '${pageContext.request.contextPath}/pictures/user-default.jpg')
             this.src = '${pageContext.request.contextPath}/pictures/user-default.jpg';">
        </p>

        <form method="post" action="${pageContext.request.contextPath}/regged/photo" enctype="multipart/form-data">
            <p><input type="file" name="file" align="center"/></p>

            <p><input type="submit" value="send" class="btn btn-custom"></p>
        </form>

        <a href="${pageContext.request.contextPath}/uploadPhoto" class="btn btn-custom">Download</a>

    </div>
</div>
