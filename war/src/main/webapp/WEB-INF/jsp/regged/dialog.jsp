<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-4 "><h3 align="left">in developing</h3></div>
    <div class="col-md-4 " id="custom-bootstrap-window1">
        <h3 align="left">Dialogs </h3>

        <c:forEach var="message" items="${messages}">
            <p style="background-color:#CCC;">
                <a href="${pageContext.request.contextPath}/home/${message.author.id}">
                    <img src="${pageContext.request.contextPath}/imageDisplay/${message.author.id}" alt="photo"
                         width="50" height="40"
                         style="float: left"
                         onerror="if (this.src != '${pageContext.request.contextPath}/pictures/user-default.jpg')
                      this.src = '${pageContext.request.contextPath}/pictures/user-default.jpg';">

                    <span class="wallName"> ${message.author.firstName} </span>
                </a>
               <span class="wallTime">
                   <%--TODO time not working!--%>
                   <%-- &nbsp; <fmt:formatDate type="both"
                                           dateStyle="short" timeStyle="short" value="${message.getTime()}"/>--%>
                </span><br>
                <span class="wallComment" style="font-weight: 600;">
                        ${message.text}
                </span>
            </p>
        </c:forEach>

        <form action="${pageContext.request.contextPath}/regged/messages/${friend.id}" method="post">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-8">
                        <input name="text"
                               placeholder="Hello there!"
                               class="form-control">
                    </div>

                    <div class="col-md-3 col-md-offset-1">
                        <input type="submit"
                               alt="ok" class="btn btn-custom" name="ok" value="ok">
                    </div>
                </div>
            </div>
        </form>

    </div>
    <div class="col-md-4 ">
        <img src="${pageContext.request.contextPath}/pictures/branding_bot.png">
    </div>
</div>