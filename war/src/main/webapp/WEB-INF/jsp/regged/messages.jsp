<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<div class="row">
    <div class="col-md-4 "><h3 align="left">Unite and communicate!</h3></div>
    <div class="col-md-4 " id="custom-bootstrap-window1">
        <h3 align="left">Dialogs </h3>

        <c:forEach var="person" items="${friends}">
            <p style="background-color:#CCC; ">
                <a href="${pageContext.request.contextPath}/regged/messages/${person.id}">

                    <img src="${pageContext.request.contextPath}/imageDisplay/${person.id}" alt="photo"
                         width="50" height="40"
                         style="float: left"
                         onerror="if (this.src != '${pageContext.request.contextPath}/pictures/user-default.jpg')
                      this.src = '${pageContext.request.contextPath}/pictures/user-default.jpg';">

                    <span class="wallName" style="text-decoration-color: black;"> ${person.getFullName()} </span>

                <span class="wallComment" style="font-weight: 600;text-decoration: none;">
                        <br>Start dialog with ${person.firstName}
                </span>
                </a>
            </p>
        </c:forEach>

    </div>
    <div class="col-md-4 ">
        <img src="${pageContext.request.contextPath}/pictures/branding_bot.png">
    </div>
</div>