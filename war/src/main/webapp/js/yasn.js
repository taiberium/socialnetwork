/**
 * Created by Taiberium on 30.09.2015.
 */
function isOnline(personId, onlineId) {
    $.get(context + "/statusDisplay/" + personId, function (data) {
        $("#" + onlineId + " h5").remove();
        var status = data ? "online" : "offline";
        $("#" + onlineId).append("<h5>" + status + "</h5>");
    });
}

function getPersons() {
    $.get(context + "/scriptSearch?" + $("#searchForm").serialize(), function (data) {
        $("#profiles div").remove();
        for (var i = 0; i < data.length; i++) {
            $("#profiles").append(printPerson(data[i]));
        }
    })
}

function printPerson(person) {
    var firstName = person.firstName !== null ? person.firstName : '';
    var lastName = person.lastName !== null ? person.lastName : '';

    var photo = "<img src=\"" + context + "/imageDisplay/" + person.id + "\" " +
        "class=\"img-rounded\" alt=\"photo\" width=\"50\" height=\"40\" style=\"float: left\"" +
        "onerror=\"if (this.src != '" + context + "/pictures/user-default.jpg')" +
        "this.src = '" + context + "/pictures/user-default.jpg';\">";

    if (firstName || lastName) {
        return " <div style='background-color:#CCC; min-height: 40px' >" +
            "<a style='text-decoration: none; color: #000000;' href='" + context + "/home/" + person.id + "'>" +
            photo +
            "<p>" + firstName + " " + lastName + "</p> </a> </div>";
    } else {
        return '';
    }
}

function getCities(countrySelectId, citySelectId) {

    var countrySelector = document.getElementById(countrySelectId);
    var countryId = countrySelector.options[countrySelector.selectedIndex].value;

    if (countryId && citySelectId) {
        $.get(context + "/cityDisplay/" + countryId, function (data) {
            $("#" + citySelectId + " option").val(null).remove();
            $("#" + citySelectId).append("<option value=''> ... </option>");
            for (var i = 0; i < data.length; i++) {
                $("#" + citySelectId).append("<option value='" + data[i].id + "'> " + data[i].name + "</option>");
            }
        })
    }
}