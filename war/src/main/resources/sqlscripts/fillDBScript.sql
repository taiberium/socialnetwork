﻿insert into person (first_name,last_name,gender,email,birth, password) values ('John','Sharp','male','sharp@gmail.com' , '27/11/1985',233);

insert into person (first_name,last_name,gender,email,birth, password) values ('Karl','Winsdale','male','wins@gmail.com' , '09/05/1991',255);

insert into person (first_name,last_name,gender,email,birth, password) values ('Samuel','Klark','male', 'samKlar@gmail.com' , '11/07/1948',217);

insert into person (first_name,last_name,gender,email,birth, password) values ('Janine','Stuard','female','janine@gmail.com' , '15/03/1978',231);

insert into person (first_name,last_name,gender,email,birth, password) values ('Leena','Watson','female', 'watsUp@gmail.com' , '05/01/1989',243);

insert into country (name) VALUES ('Russian Federation');
insert into country (name) VALUES ('USA');
insert into country (name) VALUES ('Germany');
insert into country (name) VALUES ('France');

insert into city (name,country_id) VALUES ('Moscow',1);
insert into city (name,country_id) VALUES ('St.Peterburg',1);
insert into city (name,country_id) VALUES ('Kazan',1);
insert into city (name,country_id) VALUES ('Ufa',1);
insert into city (name,country_id) VALUES ('Surgut',1);

insert into city (name,country_id) VALUES ('Washington',2);
insert into city (name,country_id) VALUES ('New-York',2);
insert into city (name,country_id) VALUES ('Delaver',2);
insert into city (name,country_id) VALUES ('Denver',2);

insert into city (name,country_id) VALUES ('Hamburg',3);
insert into city (name,country_id) VALUES ('Berlin',3);
insert into city (name,country_id) VALUES ('Brandenburg',3);
insert into city (name,country_id) VALUES ('Munich',3);
insert into city (name,country_id) VALUES ('Stuttgart',3);
insert into city (name,country_id) VALUES ('Drezden',3);

insert into city (name,country_id) VALUES ('Paris',4);
insert into city (name,country_id) VALUES ('Marseille',4);
insert into city (name,country_id) VALUES ('Lyon',4);
insert into city (name,country_id) VALUES ('Nice',4);
insert into city (name,country_id) VALUES ('Strasbourg',4);
insert into city (name,country_id) VALUES ('Bordeaux',4);