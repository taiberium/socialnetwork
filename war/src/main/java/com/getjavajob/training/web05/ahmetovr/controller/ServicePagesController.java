package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;

/**
 * Created by Rustam on 30.08.2015.
 */
@Controller
public class ServicePagesController {

    private final CurrentUser currentUser;

    @Autowired
    public ServicePagesController(CurrentUser currentUser) {
        this.currentUser = currentUser;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/")
    public ModelAndView getStartPage() {
        return getModelAndView("startPage.jsp", currentUser != null && currentUser.getPerson() != null);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/help")
    public ModelAndView getHelpPage() {
        return getModelAndView("help.jsp", currentUser != null && currentUser.getPerson() != null);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/info")
    public ModelAndView getInfoPage() {
        return getModelAndView("info.jsp", currentUser != null && currentUser.getPerson() != null);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/pageNotFound")
    public ModelAndView getPageNotFound() {
        return getModelAndView("pageNotFound.jsp", currentUser != null && currentUser.getPerson() != null);
    }
}
