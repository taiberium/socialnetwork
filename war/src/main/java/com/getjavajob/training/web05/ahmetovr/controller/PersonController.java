package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumType;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;
import static com.getjavajob.training.web05.ahmetovr.helper.PersonConfigHelper.config;


/**
 * Created by Rustam on 30.08.2015.
 */
@Controller
public class PersonController {

    private final PersonService personService;
    private final SettingService settingService;
    private final CurrentUser currentUser;
    private final LocationService locationService;

    @Autowired
    public PersonController(PersonService personService, SettingService settingService,
                            CurrentUser currentUser, LocationService locationService) {
        this.personService = personService;
        this.settingService = settingService;
        this.currentUser = currentUser;
        this.locationService = locationService;
    }

    @RequestMapping(path = "/friends/{id}", method = RequestMethod.GET)
    public ModelAndView getFriends(@PathVariable Integer id) {
        Person person = personService.getPersonById(id);
        ModelAndView modelAndView = getModelAndView("friends.jsp", currentUser != null && currentUser.getPerson() != null);
        modelAndView.addObject("viewedPerson", person);
        modelAndView.addObject("friendsCount", personService.getPersonFriendsCount(person));
        modelAndView.addObject("countries", locationService.getAllCountries());
        return modelAndView;
    }

    @RequestMapping(path = "/regged/personConfig", method = RequestMethod.GET)
    public ModelAndView getPersonConfig() {
        ModelAndView modelAndView = getModelAndView("regged/personConfig.jsp", true);
        modelAndView.addObject("countries", locationService.getAllCountries());
        return modelAndView;
    }

    @RequestMapping(path = "/regged/personConfig", method = RequestMethod.POST)
    public ModelAndView postPersonConfig(@RequestParam Map<String, String> params) {
        return config(params, currentUser, personService, locationService);
    }

    @RequestMapping(path = "/regged/settings", method = RequestMethod.GET)
    public ModelAndView getSettings() {
        Person person = currentUser.getPerson();
        ModelAndView modelAndView = getModelAndView("regged/settings.jsp", true);
        PersonSettingAggregate personSetting = settingService.getPersonSettings(person);

        PersonSetting profileViewSetting = personSetting.getProfileView();
        PersonSetting friendsViewSetting = personSetting.getFriendsView();
        PersonSetting ageViewSetting = personSetting.getAgeView();
        PersonSetting wallWriteSetting = personSetting.getWallWrite();

        SettingEnumValue profileViewVal = getDisplayedValue(profileViewSetting, SettingEnumValue.ALL);
        SettingEnumValue friendsViewVal = getDisplayedValue(friendsViewSetting, SettingEnumValue.ALL);
        SettingEnumValue ageViewVal = getDisplayedValue(ageViewSetting, SettingEnumValue.ALL);
        SettingEnumValue wallWriteVal = getDisplayedValue(wallWriteSetting, SettingEnumValue.REGGED);

        modelAndView.addObject("profileViewVal", profileViewVal);
        modelAndView.addObject("friendsViewVal", friendsViewVal);
        modelAndView.addObject("ageViewVal", ageViewVal);
        modelAndView.addObject("wallWriteVal", wallWriteVal);
        return modelAndView;
    }

    private SettingEnumValue getDisplayedValue(PersonSetting setting, SettingEnumValue defaultVal) {
        if (setting == null) {
            return defaultVal;
        }
        return setting.getVal();
    }

    @RequestMapping(path = "/regged/settings", method = RequestMethod.POST)
    public String postSettings(@RequestParam Map<String, String> params) {
        String profileView = params.get("profileView");
        String friendsView = params.get("friendsView");
        String ageView = params.get("ageView");
        String wallWrite = params.get("wallWrite");
        Person person = currentUser.getPerson();
        PersonSettingAggregate personSetting = settingService.getPersonSettings(person);

        PersonSetting profileViewSetting = personSetting.getProfileView();
        PersonSetting friendsViewSetting = personSetting.getFriendsView();
        PersonSetting ageViewSetting = personSetting.getAgeView();
        PersonSetting wallWriteSetting = personSetting.getWallWrite();

        setSetting(profileViewSetting, profileView, SettingEnumType.PROFILE_VIEW);
        setSetting(friendsViewSetting, friendsView, SettingEnumType.FRIENDS_VIEW);
        setSetting(ageViewSetting, ageView, SettingEnumType.AGE_VIEW);
        setSetting(wallWriteSetting, wallWrite, SettingEnumType.WALL_WRITE);
        return "redirect:/regged/settings";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/statusDisplay/{viewedPersonId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    boolean isOnline(@PathVariable Integer viewedPersonId) {
        if (viewedPersonId != null) {
            Person viewedPerson = personService.getPersonById(viewedPersonId);
            return CurrentUser.isOnline(viewedPerson);
        }
        return false;
    }

    private PersonSetting setSetting(PersonSetting setting,
                                     String parameterVal, SettingEnumType settingType) {
        PersonSetting saveSetting = setting;
        SettingEnumValue settingValue = SettingEnumValue.getSettingValue(parameterVal);
        if (saveSetting != null) {
            if (saveSetting.getVal() != settingValue) {
                saveSetting.setVal(settingValue);
                return settingService.save(saveSetting);
            }
        } else {
            if (SettingEnumType.getDefaultValue(settingType) != settingValue) {
                saveSetting = new PersonSetting(currentUser.getPerson(), settingService.get(settingType), settingValue);
                return settingService.save(saveSetting);
            }
        }
        return saveSetting;
    }
}

