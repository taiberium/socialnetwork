package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonFile;
import com.getjavajob.training.web05.ahmetovr.service.PersonFileService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;

/**
 * Created by Taiberium on 16.09.2015.
 */
@Controller
public class FileController {

    private final PersonFileService personFileService;
    private final CurrentUser currentUser;
    private final PersonService personService;

    @Autowired
    public FileController(PersonFileService personFileService, CurrentUser currentUser, PersonService personService) {
        this.personFileService = personFileService;
        this.currentUser = currentUser;
        this.personService = personService;
    }

    @RequestMapping(path = "/regged/photo", method = RequestMethod.GET)
    public ModelAndView imageConfig() {
        return getModelAndView("regged/photoView.jsp", true);
    }

    @RequestMapping(path = "/regged/photo", headers = "content-type=multipart/*", method = RequestMethod.POST)
    public ModelAndView imageConfig(@RequestParam("file") MultipartFile file) throws IOException {
        Person owner = currentUser.getPerson();
        PersonFile currentAvatar = personFileService.getByOwner(owner);
        PersonFile personFile = currentAvatar == null ? new PersonFile() : currentAvatar;
        personFile.setName(file.getOriginalFilename());
        personFile.setData(file.getBytes());
        personFile.setOwner(owner);
        personFileService.save(personFile);
        return getModelAndView("regged/photoView.jsp", true);
    }

    @RequestMapping(value = "/imageDisplay/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public
    @ResponseBody
    byte[] getImage(@PathVariable int id) throws IOException {
        Person owner = personService.getPersonById(id);
        PersonFile personFile = personFileService.getByOwner(owner);
        return personFile != null ? personFile.getData() : null;
    }

    @RequestMapping(value = "/uploadPhoto", method = RequestMethod.GET)
    public ResponseEntity<byte[]> uploadImage() throws IOException {
        PersonFile file = personFileService.getByOwner(currentUser.getPerson());
        if (file == null) {
            return null;
        }
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        httpHeaders.set("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
        httpHeaders.setContentLength(file.getData().length);
        return new ResponseEntity<>(file.getData(), httpHeaders, HttpStatus.CREATED);
    }
}
