package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.SearchFilter;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.helper.SearchHelper;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;
import static com.getjavajob.training.web05.ahmetovr.helper.SearchHelper.getSearchFilterBuilder;

/**
 * Created by Taiberium on 30.09.2015.
 */
@Controller
public class SearchController {

    private final PersonService personService;
    private final LocationService locationService;

    @Autowired
    public SearchController(PersonService personService, LocationService locationService) {
        this.personService = personService;
        this.locationService = locationService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/scriptSearch", produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    Collection<Person> getScriptSearch(@RequestParam Map<String, String> params) {
        String stringPersonId = params.get("viewedPerson");
        SearchFilter searchFilter = getSearchFilterBuilder(params, locationService);
        if (stringPersonId != null && !stringPersonId.isEmpty()) {
            int personId = Integer.parseInt(stringPersonId);
            Person viewedPerson = personService.getPersonById(personId);
            searchFilter.setPerson(viewedPerson);
        }
        Collection<Person> originalPersonCollection = personService.getPersonByFilter(searchFilter);
        return SearchHelper.getSavePersonCollection(originalPersonCollection);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/regged/search")
    public ModelAndView getSearch() {
        ModelAndView modelAndView = getModelAndView("regged/search.jsp", true);
        modelAndView.addObject("personsNumber", personService.getPersonsNumber());
        modelAndView.addObject("countries", locationService.getAllCountries());
        return modelAndView;
    }
}
