package com.getjavajob.training.web05.ahmetovr.filters;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Rustam on 24.07.2015.
 */

public class ReggedFilter implements Filter {

    private CurrentUser user;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        if (user == null || user.getPerson() == null) {
            String contextPath = ((HttpServletRequest) request).getContextPath();
            httpServletResponse.sendRedirect(contextPath + "/login");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }

    @Autowired
    public void setUser(CurrentUser currentUser) {
        user = currentUser;
    }
}
