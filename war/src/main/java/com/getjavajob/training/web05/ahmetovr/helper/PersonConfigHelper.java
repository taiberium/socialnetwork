package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.RedirectHelper.redirectWithLogin;

/**
 * Created by Taiberium on 14.09.2015.
 */

public class PersonConfigHelper {

    public static ModelAndView config(Map<String, String> params, CurrentUser currentUser,
                                      PersonService personService, LocationService locationService) {

        final String firstName = params.get("firstName");
        final String lastName = params.get("lastName");
        final String email = params.get("email");
        final String password = params.get("password");
        final String gender = params.get("sex");
        final String birthString = params.get("birth");
        final String cityId = params.get("cityId");
        String registrationParameter = params.get("registration");

        ModelAndView modelAndView = new ModelAndView("mainPage");
        boolean registration = registrationParameter != null && !registrationParameter.isEmpty();

        modelAndView.addObject("firstName", firstName);
        modelAndView.addObject("lastName", lastName);
        modelAndView.addObject("email", email);
        modelAndView.addObject("password", password);
        modelAndView.addObject("sex", gender);
        modelAndView.addObject("firstName", firstName);
        modelAndView.addObject("registration", registration);

        City selectedCity = null;
        if (cityId != null && cityId.length() > 0) {
            selectedCity = locationService.getCity(Integer.parseInt(cityId));
        }

        Person person;
        if (!registration) {
            person = currentUser.getPerson();
        } else {
            if (email == null || email.length() == 0 || password == null || password.length() == 0) {
                modelAndView.addObject("path", "registration.jsp");
                modelAndView.addObject("message", "not all parameters was set");
                modelAndView.addObject("regged", false);
                return modelAndView;
            }
            person = new Person(email, password);
        }

        Date birth = null;
        if (birthString != null && birthString.length() > 0) {
            try {
                Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(birthString);
                birth = new Date(utilDate.getTime());
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        if (personService.getPersonByEmail(email) != null) {
            modelAndView.addObject("path", registration ? "registration.jsp" : "regged/personConfig.jsp");
            modelAndView.addObject("regged", !registration);
            modelAndView.addObject("message", " already exist! ");
            return modelAndView;
        }
        if (gender != null && gender.length() > 0 && !"empty".equals(gender)) {
            person.setGender(gender);
        }
        if (birth != null) {
            person.setBirth(birth);
        }
        if (selectedCity != null) {
            person.setLocation(selectedCity);
        }
        if (firstName != null && firstName.length() > 0) {
            person.setFirstName(firstName);
        }
        if (lastName != null && lastName.length() > 0) {
            person.setLastName(lastName);
        }
        if (email != null && email.length() > 0) {
            person.setEmail(email);
        }
        if (password != null && password.length() > 0) {
            person.setPassword(password);
        }

        Person savedPerson = personService.save(person);
        return registration ? redirectWithLogin(savedPerson, currentUser) : new ModelAndView("redirect:/regged/personConfig");
    }
}
