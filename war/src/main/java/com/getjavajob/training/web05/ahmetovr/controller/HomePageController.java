package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.WallRecord;
import com.getjavajob.training.web05.ahmetovr.helper.SettingsHelper;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import com.getjavajob.training.web05.ahmetovr.service.WallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;

/**
 * Created by Rustam on 30.08.2015.
 */
@Controller
public class HomePageController {

    private final PersonService personService;
    private final SettingService settingService;
    private final WallService wallService;
    private CurrentUser currentUser;

    @Autowired
    public HomePageController(PersonService personService, SettingService settingService,
                              WallService wallService, CurrentUser currentUser) {
        this.personService = personService;
        this.settingService = settingService;
        this.wallService = wallService;
        this.currentUser = currentUser;
    }

    @RequestMapping(path = "/home/{id}", method = RequestMethod.GET)
    public ModelAndView getHomePage(@PathVariable Integer id) {
        ModelAndView modelAndView = getModelAndView("homePage.jsp");
        Person reggedViewer = currentUser.getPerson();
        Person viewedPerson = personService.getPersonById(id);
        //Display
        displayPerson(modelAndView, viewedPerson);
        //Settings
        new SettingsHelper(settingService, currentUser, personService).setSettings(modelAndView, viewedPerson);
            if (reggedViewer != null) {
                //owner have full access to profile
                if (viewedPerson.getId() == reggedViewer.getId()) {
                    modelAndView.addObject("privateView", true);
                } else {
                    modelAndView.addObject("privateView", false);
                }
                modelAndView.addObject("isFriends", personService.isFriends(reggedViewer, viewedPerson));
                // regged viewer can have reggedMenu
                modelAndView.addObject("regged", true);
            } else {
                modelAndView.addObject("regged", false);
                modelAndView.addObject("settingsView", false);
            }
        return modelAndView;
    }

    @RequestMapping(path = "/home", method = RequestMethod.POST)
    public ModelAndView postWallRecord(@RequestParam Map<String, String> params) {
        String wallMessage = params.get("newWall");
        int ownerId = Integer.parseInt(params.get("personView"));
        Person owner = personService.getPersonById(ownerId);
        WallRecord wallRecord = new WallRecord(wallMessage, currentUser.getPerson(), owner);
        wallService.save(wallRecord);
        return new ModelAndView("redirect:/home/" + ownerId);
    }

    @RequestMapping(path = "/regged/friendsChange", method = RequestMethod.POST)
    public String makeFriends(@RequestParam Map<String, String> params) {
        int ownerId = Integer.parseInt(params.get("personView"));
        Person pageOwner = personService.getPersonById(ownerId);
        Person currentUserPerson = currentUser.getPerson();
        String buttonValue = params.get("button");
        if (buttonValue.contains("make")) {
            personService.makeFriends(currentUserPerson, pageOwner);
        } else {
            personService.removeRelation(currentUserPerson, pageOwner);
        }
        return "redirect:/home/" + ownerId;
    }

    private void displayPerson(ModelAndView modelAndView, Person person) {
        String fullName = "";
        if (person.getFirstName() != null && person.getLastName() != null) {
            fullName = person.getFirstName() + " " + person.getLastName();
        } else if (person.getFirstName() != null) {
            fullName = person.getFirstName();
        } else if (person.getLastName() != null) {
            fullName = person.getLastName();
        }
        modelAndView.addObject("fullName", fullName);
        Long friendsCount = personService.getPersonFriendsCount(person);
        modelAndView.addObject("friendsCount", friendsCount);
        Collection<WallRecord> wallRecordCollection = wallService.getPersonWall(person);
        modelAndView.addObject("wallRecordCollection", wallRecordCollection);
        //Display
        modelAndView.addObject("personView", person);
        modelAndView.addObject("age", personService.getYears(person));
    }
}
