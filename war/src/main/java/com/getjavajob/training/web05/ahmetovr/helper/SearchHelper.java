package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.SearchFilter;
import com.getjavajob.training.web05.ahmetovr.bean.City;
import com.getjavajob.training.web05.ahmetovr.bean.Country;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Rustam on 25.08.2015.
 */
public class SearchHelper {

    public static Collection<Person> getSavePersonCollection(Collection<Person> personCollection) {
        Collection<Person> savePersonCollection = new ArrayList<>();
        for (Person person : personCollection) {
            Person personSaveCopy = new Person(person);
            savePersonCollection.add(personSaveCopy);
        }
        return savePersonCollection;
    }

    public static SearchFilter getSearchFilterBuilder(Map<String, String> params,
                                                      LocationService locationService) {
        SearchFilter searchFilter = new SearchFilter();
        String name = params.get("name");
        if (name != null && name.length() > 0) {
            String[] names = name.split(" ");
            searchFilter.setName(names[0]);
            if (names.length > 1) {
                searchFilter.setLastName(names[1]);
            }
        }
        String ageFrom = params.get("ageFrom");
        String ageTo = params.get("ageTo");
        if (ageFrom != null && ageTo != null && ageFrom.length() > 0 && ageTo.length() > 0) {
            searchFilter.setAge(Integer.parseInt(ageFrom), Integer.parseInt(ageTo));
        }
        String stringCountryId = params.get("countryId");
        if (stringCountryId != null && !stringCountryId.isEmpty()) {
            Country country = locationService.getCountry(Integer.parseInt(stringCountryId));
            searchFilter.setCountry(country);
        }
        String stringCityId = params.get("cityId");
        if (stringCityId != null && !stringCityId.isEmpty()) {
            City city = locationService.getCity(Integer.parseInt(stringCityId));
            searchFilter.setCity(city);
        }
        String sex = params.get("sex");
        if (!"any".equalsIgnoreCase(sex)) {
            searchFilter.setSex(sex);
        }
        return searchFilter;
    }
}
