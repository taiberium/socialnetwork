package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.service.LocationService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Map;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;
import static com.getjavajob.training.web05.ahmetovr.helper.PersonConfigHelper.config;
import static com.getjavajob.training.web05.ahmetovr.helper.RedirectHelper.redirectWithLogin;

/**
 * Created by Rustam on 29.08.2015.
 */
@Controller
public class AuthorisationController {

    private final PersonService personService;
    private final LocationService locationService;
    private final CurrentUser currentUser;

    @Autowired
    public AuthorisationController(PersonService personService, LocationService locationService,
                                   CurrentUser currentUser) {
        this.personService = personService;
        this.locationService = locationService;
        this.currentUser = currentUser;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/login")
    public ModelAndView getLogin() {
        return getModelAndView("login.jsp", false);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/login")
    public ModelAndView postLogin(@RequestParam("email") String email, @RequestParam("password") String password) {

        ModelAndView modelAndView = getModelAndView("login.jsp", false);
        final Person person = personService.getPersonByEmail(email);
        if (person == null) {
            modelAndView.addObject("wrongUser", "no such user or wrong password");
            return modelAndView;
        } else if (person.getPassword().equals(password)) {
            return redirectWithLogin(person, currentUser);
        } else {
            modelAndView.addObject("wrongUser", "no such user or wrong password");
            return modelAndView;
        }
    }

    //TODO listener for invalidation
    @RequestMapping(method = RequestMethod.GET, path = "/logout")
    public String logout(HttpSession session) {
        if (currentUser.getPerson() != null) {
            CurrentUser.resetOnline(currentUser.getPerson());
        }
        session.invalidate();
        return "redirect:/login";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/registration")
    public ModelAndView getRegistration() {
        ModelAndView modelAndView = getModelAndView("registration.jsp", false);
        modelAndView.addObject("registration", "registration");
        modelAndView.addObject("countries", locationService.getAllCountries());
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/registration")
    public ModelAndView postRegistration(@RequestParam Map<String, String> params) {
        return config(params, currentUser, personService, locationService);
    }

}
