package com.getjavajob.training.web05.ahmetovr;

import com.getjavajob.training.web05.ahmetovr.bean.Person;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Taiberium on 10.09.2015.
 */
public class CurrentUser implements Serializable {

    private final static HashMap<Integer, Person> loggedPersons = new HashMap<>();
    private Person person;

    public static boolean isOnline(Person person) {
        return loggedPersons.containsKey(person.getId());
    }

    public static synchronized void setOnline(Person person) {
        loggedPersons.put(person.getId(), person);
    }

    public static synchronized void resetOnline(Person person) {
        loggedPersons.remove(person.getId());
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
