package com.getjavajob.training.web05.ahmetovr.helper;

import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Taiberium on 18.09.2015.
 */
public class ModelAndViewHelper {

    public static ModelAndView getModelAndView(String path, boolean regged) {
        ModelAndView modelAndView = new ModelAndView("mainPage", "path", path);
        modelAndView.addObject("regged", regged);
        return modelAndView;
    }

    public static ModelAndView getModelAndView(String path) {
        return new ModelAndView("mainPage", "path", path);
    }
}
