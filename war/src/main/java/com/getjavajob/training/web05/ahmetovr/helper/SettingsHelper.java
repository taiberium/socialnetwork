package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.PersonSettingAggregate;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.bean.PersonSetting;
import com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import com.getjavajob.training.web05.ahmetovr.service.SettingService;
import org.springframework.web.servlet.ModelAndView;

import static com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue.ALL;
import static com.getjavajob.training.web05.ahmetovr.enums.SettingEnumValue.REGGED;

/**
 * Created by Taiberium on 30.09.2015.
 */
public class SettingsHelper {

    private final SettingService settingService;
    private final CurrentUser currentUser;
    private final PersonService personService;

    public SettingsHelper(SettingService settingService, CurrentUser currentUser, PersonService personService) {
        this.settingService = settingService;
        this.currentUser = currentUser;
        this.personService = personService;
    }

    public void setSettings(ModelAndView modelAndView, Person viewedPerson) {

        PersonSettingAggregate personSettings = settingService.getPersonSettings(viewedPerson);
        PersonSetting profileViewSetting = personSettings.getProfileView();
        PersonSetting friendsViewSetting = personSettings.getFriendsView();
        PersonSetting ageViewSetting = personSettings.getAgeView();
        PersonSetting wallWriteSetting = personSettings.getWallWrite();

        //set view settings
        boolean profileView = isVisible(viewedPerson, profileViewSetting, ALL);
        boolean friendsView = isVisible(viewedPerson, friendsViewSetting, ALL);
        boolean ageView = isVisible(viewedPerson, ageViewSetting, ALL);
        boolean wallWriteView = isVisible(viewedPerson, wallWriteSetting, REGGED);

        //Security settings
        modelAndView.addObject("profileView", profileView);
        modelAndView.addObject("friendsView", friendsView);
        modelAndView.addObject("ageView", ageView);
        modelAndView.addObject("wallWrite", wallWriteView);
    }

    public boolean isVisible(Person viewedPerson, PersonSetting setting, SettingEnumValue defaultValue) {
        SettingEnumValue val;
        Person reggedViewer = currentUser.getPerson();
        if (setting == null) {
            val = defaultValue;
        } else {
            val = setting.getVal();
        }
        switch (val) {
            case REGGED:
                return reggedViewer != null;
            case FRIENDS:
                return personService.isFriends(viewedPerson, reggedViewer) || viewedPerson.equals(reggedViewer);
            case NONE:
                return viewedPerson.equals(reggedViewer);
            default:
                return true;
        }
    }
}
