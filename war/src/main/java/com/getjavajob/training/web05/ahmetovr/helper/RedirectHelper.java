package com.getjavajob.training.web05.ahmetovr.helper;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Rustam on 25.07.2015.
 */
public class RedirectHelper {

    public static ModelAndView redirectWithLogin(Person person, CurrentUser currentUser) {
        CurrentUser.setOnline(person);
        currentUser.setPerson(person);
        return new ModelAndView("redirect:/home/" + person.getId());
    }
}
