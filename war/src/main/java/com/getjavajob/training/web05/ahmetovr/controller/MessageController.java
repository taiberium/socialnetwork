package com.getjavajob.training.web05.ahmetovr.controller;

import com.getjavajob.training.web05.ahmetovr.CurrentUser;
import com.getjavajob.training.web05.ahmetovr.bean.Message;
import com.getjavajob.training.web05.ahmetovr.bean.Person;
import com.getjavajob.training.web05.ahmetovr.service.MessageService;
import com.getjavajob.training.web05.ahmetovr.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;

import static com.getjavajob.training.web05.ahmetovr.helper.ModelAndViewHelper.getModelAndView;

/**
 * Created by Taiberium on 19.10.2015.
 */
@Controller
public class MessageController {

    MessageService messageService;
    CurrentUser currentUser;
    PersonService personService;

    @Autowired
    public MessageController(MessageService messageService, CurrentUser currentUser,
                             PersonService personService) {
        this.messageService = messageService;
        this.currentUser = currentUser;
        this.personService = personService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/regged/messages")
    public ModelAndView getMessages() {
        Person user = currentUser.getPerson();
        Collection<Person> friends = personService.getPersonFriends(user);
        ModelAndView modelAndView = getModelAndView("regged/messages.jsp", true);
        modelAndView.addObject("friends", friends);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/regged/messages/{personId}")
    public ModelAndView getMessages(@PathVariable int personId) {
        Person owner = currentUser.getPerson();
        Person friend = personService.getPersonById(personId);
        Collection<Message> messages = messageService.getMessagesByPersons(owner, friend);
        ModelAndView modelAndView = getModelAndView("regged/dialog.jsp", true);
        modelAndView.addObject("friend", friend);
        modelAndView.addObject("messages", messages);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/regged/messages/{personId}")
    public ModelAndView postMessage(@PathVariable int personId, @RequestParam String text) {
        Person author = currentUser.getPerson();
        Person opponent = personService.getPersonById(personId);
        messageService.save(new Message(text, author, opponent));
        return new ModelAndView("redirect:/regged/messages/" + personId);
    }
}
